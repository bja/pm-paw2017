module C_FFTW_MPI {

  use MPI;
  use FFTW;
  use SysCTypes;
  require "fftw3-mpi.h";

  config const numFFTWThreads=0;
  config const FFTWPlanningTimeout: c_double=600.0;

  coforall loc in Locales do on loc {
      fftw_init_threads();
      fftw_mpi_init();

      const nth = if numFFTWThreads > 0 then numFFTWThreads : c_int
        else here.maxTaskPar : c_int;
      fftw_plan_with_nthreads(nth);

      // If we use PATIENT, this is useful
      fftw_set_timelimit(FFTWPlanningTimeout); 
  }


  // Cleanup module
  proc deinit() {
    coforall loc in Locales do on loc {
        fftw_mpi_cleanup();
        fftw_cleanup_threads();
      }
  }

  // Define an enum for different planner flags
  enum Planner {Estimate, Measure, Patient};

  proc translateFFTWPlanner(flag : Planner) {
    select flag {
      when Planner.Estimate do return FFTW_ESTIMATE;
      when Planner.Measure do return FFTW_MEASURE;
      when Planner.Patient do return FFTW_PATIENT;
      otherwise halt("Unknown planner");
      }
  }


  // Extern definitions go here
  extern const FFTW_MPI_TRANSPOSED_IN : c_uint;
  extern const FFTW_MPI_TRANSPOSED_OUT : c_uint;
  extern proc fftw_set_timelimit(seconds : c_double);
  extern proc fftw_init_threads(): int;
  extern proc fftw_cleanup_threads();
  extern proc fftw_plan_with_nthreads(nthreads: c_int);
  extern proc fftw_mpi_init();
  extern proc fftw_mpi_cleanup();
  extern proc fftw_mpi_plan_dft_r2c_3d(n0 : c_ptrdiff, n1 : c_ptrdiff, n2 : c_ptrdiff,
                                       ref inarr , ref outarr,
                                       comm : MPI_Comm, flags : c_uint) : fftw_plan;
  extern proc fftw_mpi_plan_dft_c2r_3d(n0 : c_ptrdiff, n1 : c_ptrdiff, n2 : c_ptrdiff,
                                       ref inarr, ref outarr,
                                       comm : MPI_Comm, flags : c_uint) : fftw_plan;

  // End module
}