// Module that handles cosmological parameters, box sizes etc
// and related routines...

module Cosmo {

  use Utils;
  use ndfilehandler;

  // Basic cosmological parameters
  config const
    omegam=0.31,
    omegax=0.69,
    weff=-1.0;
  const omegak=1.0-omegam-omegax;

  // Box side in Mpc/h
  config const Lbox=512.0;
  config const zinit=30.0, zfinal=0.0;

  // For simplicity, put the box parameters here as well
  config const Ng=512;
  config const Nc=512; // The number of particles is Nc**3
  const Npart = Nc**3;

  // Define the source of the initial power spectrum
  config const PkIC="pkic_0.0323";

  ////////////////////////////////////////////////////////////////////////

  const ainit = 1.0/(1+zinit),
    afinal = 1.0/(1+zfinal);

  // Sanity checks
  if (omegam < 0.1) || (omegam > 1.0) ||
    (omegax < 0.0) || (omegax > 1.0) ||
    (weff < -1.3)  || (weff > -0.7)
  {
    writeln("ERROR : Odd choices of parameters....");
    exit(1234);
  }

  if (Ng%numLocales != 0) then
    halt("Error! Ng must be divisible by numLocales");
  if (Ng%2 != 0) then
    halt("Error! Ng must be divisible by 2");
  if (Nc%numLocales != 0) then
    halt("Error! Nc must be divisible by numLocales");

  // Interpolating functions for the symplectic integrator
  config const debugTimeIntegrals=true;
  const (symx, symp) = settimeintegral(ainit, afinal);
  if debugTimeIntegrals {
    var ff = openwriter("timeIntegrals.debug");
    ff.writef("# Debug output for time integrals \n");
    ff.writef("# OmegaM = %r, OmegaX = %r, weff = %r \n", omegam, omegax, weff);
    const Na = 200;
    const dloga = log(afinal/ainit)/Na;
    for ia in 0..Na {
      const loga = log(ainit) + ia*dloga;
      const xx = symx(loga);
      const pp = symp(loga);
      ff.writef("%15.4er %15.6er %15.6er\n",loga, xx, pp);
    }
    ff.close();
  }



  /************************************
   Functions that are useful for the symplectic integrator
  ************************************/

  // Time integrand
  // NOTE : The notation below is a little horrid, we use
  // da and a when we really mean dloga and loga
  proc timeintegrand(loga: real):real {
    var aa = exp(loga);
    var iaa = 1.0/aa;
    var Ea2 = omegam*iaa**3 + omegak*iaa**2 + omegax*exp(-3.0*(1+weff)*loga);
    return aa**2 * sqrt(Ea2);
  }

  proc settimeintegral(Ainitial: real, Afinal: real, NtableTime:int=512) :
    (LinearInterpolation, LinearInterpolation) {
    var sym_t, sym_x, sym_p: [0.. #NtableTime]real;
    const Nint=500;

    const ainit = log(0.9*Ainitial),
      afinal= log(1.1*Afinal),
      da = (afinal-ainit)/(NtableTime-1.0),
      hh = da/Nint;
    sym_t[0] = ainit;
    sym_x[0] = 0.0;
    sym_p[0] = 0.0;

    for jj in 1..(NtableTime-1) {
      var amin = ainit + (jj-1)*da,
        amax = ainit + jj*da;

      var t1 = timeintegrand(amin),
        t2 = timeintegrand(amax),
        sumx = 1.0/t1 + 1.0/t2,
        sump = t1 + t2;


      // Simpson's rule
      var wt=4;
      for nn in 1..(Nint-1) {
        var loga = amin + nn*hh;
        var ta = timeintegrand(loga);
        sumx += wt/ta;
        sump += wt*ta;
        wt = 8/wt;
      }
      sym_t[jj] = amax;
      sym_x[jj] = sym_x[jj-1] + sumx*hh/3.0;
      sym_p[jj] = sym_p[jj-1] + sump*hh/3.0;
    }

    var symx = new LinearInterpolation(sym_t, sym_x);
    var symp = new LinearInterpolation(sym_t, sym_p);

    return (symx, symp);
  }



  proc loadInitialPowerSpectrum(NpkTable:int=2000000) {
    var kval = readColumn(real(64), 1,"%s/kval".format(PkIC));
    var pkval = readColumn(real(64), 1,"%s/pk".format(PkIC));

    // Do some sanity checks
    const kFund = 2.0*pi/Lbox;
    const kNyq = pi * Ng/Lbox;
    if kval[kval.domain.first] >= kFund then
      halt("Error! Input power spectrum must cover fundamental frequency");
    if kval[kval.domain.last] <= kNyq then
      halt("Error! Input power spectrum must cover Nyquist frequency");


    // Now set up table
    const dk = (1.05*Ng - 0.95*2)*pi/(NpkTable-1.0);
    var kk, pk: [0.. #NpkTable] real;

    // Fill in table
    kval = log(kval*Lbox);
    pkval = sqrt(pkval/Lbox**3);
    var acc = gsl_interp_accel_alloc();
    var spline = gsl_spline_alloc(gsl_interp_cspline, kval.size: size_t);
    gsl_spline_init(spline, kval, pkval, kval.size: size_t);

    // The GSL spline functions are not necessarily thread-safe, so we just do this
    // serially
    for ik in 0.. #NpkTable {
      kk[ik] = 1.9*pi + dk*ik;
      pk[ik] = gsl_spline_eval(spline, log(kk[ik]), acc);
    }

    // Clean up
    gsl_spline_free(spline);
    gsl_interp_accel_free(acc);

    return new LinearInterpolation(kk, pk);
  }

}