module ParticleUtils {
  use Skyline;

  config const overloadFactor=2;
  param Ndim=3;

  class Particles {
    var r, p, f: Ndim*SkylineArr(real(32));
    var pid: SkylineArr(int);

    proc Particles(ntot: int) {
      for param idim in 1..Ndim {
        r(idim) = new SkylineArr(real(32), ntot, overloadFactor=overloadFactor);
        p(idim) = new SkylineArr(real(32), ntot, overloadFactor=overloadFactor);
        f(idim) = new SkylineArr(real(32), ntot, overloadFactor=overloadFactor);
      }
      pid = new SkylineArr(int, ntot, overloadFactor=overloadFactor);
    }

    proc deinit() {
      for param idim in 1..Ndim {
        delete r(idim);
        delete p(idim);
        delete f(idim);
      }
      delete pid;
    }

    proc size(): int {
      return r(1).size();
    }


    iter Coords() ref {
      halt("Dummy serial iterator");
    }

    iter Coords(ref a: SkylineArr(?T)) ref {
      halt("Dummy serial iterator");
    }

    /* Provide parallel iterators that loop over the coordinates together.

       An optional overload allows one to also simultaneously loop over
       an additional vector.

    */

    // This would be a simple standalone iterator, but the compiler has a
    // bug dealing with reduce intents on this, so we implement as leader/follower instead.
    /* iter Coords(param tag, a ...?n) ref where tag==iterKind.standalone { */
    /*   forall tup in zip(r(1),r(2),r(3),(...a)) { */
    /*     yield tup; */
    /*   } */
    /* } */

    iter Coords(param tag) ref where tag==iterKind.leader {
      coforall loc in Locales do
        on loc {
          const t1 = here.id..here.id;
          const locDom = r(1).getLocalDomain();
          for followThis in locDom._value.these(tag) do
            yield (t1,followThis(1));
        }
    }

    iter Coords(param tag, followThis) ref where tag==iterKind.follower && followThis.size==2 {
      // Optimize in the case where everything is local
      const (r1, r2) = followThis;
      if (r1.size > 1) then halt("Unimplemented follower iteration!");
      const locid = r1.low;
      if (locid != here.id) then halt("I won't follow non-locally!"); 

      // Get local arrays and trim to the appropriate domain
      ref arr1 = r(1).getLocalBackingArray()[r2];
      ref arr2 = r(2).getLocalBackingArray()[r2];
      ref arr3 = r(3).getLocalBackingArray()[r2];

      for tup in zip(arr1, arr2, arr3) do yield tup;
    }

    iter Coords(param tag, ref a: SkylineArr(?T)) ref where tag==iterKind.leader {
      // We should check to see if a has the right shape

      coforall loc in Locales do
        on loc {
          const t1 = here.id..here.id;
          const locDom = r(1).getLocalDomain();
          for followThis in locDom._value.these(tag) do
            yield (t1,followThis(1));
        }
    }

    iter Coords(param tag, ref a: SkylineArr(?T), followThis) ref where tag==iterKind.follower && followThis.size==2 {
      // Optimize in the case where everything is local
      const (r1, r2) = followThis;
      if (r1.size > 1) then halt("Unimplemented follower iteration!");
      const locid = r1.low;
      if (locid != here.id) then halt("I won't follow non-locally!"); 

      // Get local arrays and trim to the appropriate domain
      ref arr1 = r(1).getLocalBackingArray()[r2];
      ref arr2 = r(2).getLocalBackingArray()[r2];
      ref arr3 = r(3).getLocalBackingArray()[r2];
      ref arr4 = a.getLocalBackingArray()[r2];

      for tup in zip(arr1, arr2, arr3, arr4) do yield tup;
    }


    proc printParticleStatsDim(idim: int) {
      const np = this.size();
      var xav, xmin, xmax, vav, vmin, vmax : real(32);
      forall (x1,v1) in zip(r(idim), p(idim)) with (+ reduce xav,
                                                          + reduce vav,
                                                          max reduce xmax,
                                                          max reduce vmax,
                                                          min reduce xmin,
                                                          min reduce vmin) {
        xav += x1;
        vav += abs(v1);
        if (x1 > xmax) then xmax = x1;
        if (v1 > vmax) then vmax = v1;
        if (x1 < xmin) then xmin = x1;
        if (v1 < vmin) then vmin = v1;
      }

     
      writef("x(%i) = %dr, %dr, %dr \n", idim, xmin, xav/np, xmax);
      writef("v(%i) = %dr, %dr, %dr \n", idim, vmin, vav/np, vmax);
    }


    proc printParticleStats() {
      for idim in 1..Ndim {
        this.printParticleStatsDim(idim);
      }
    }




    // End of Particle Record
  }

  /*
    Wrap particle positions into [0,1)^3

    Keep this generic...
  */
  inline proc periodic(in x:?T) {
    x -= floor(x);
    if (x == 1) then x=0:T;
    return x;
  }

  proc wrapPeriodic(PP: Particles) {
    for param idim in 1..Ndim do
      [x1 in PP.r(idim)] x1 = periodic(x1);
  }

  /*
    Slab decomposition.

    This routine derives heavily from the ISx benchmark example.

    NOTES:
    1. Assumes that the particles are in [0,1)^3 -- i.e. in box units.
    2. The timing is done in all cases, which pays the cost of a global reduce.
   */
  proc slabDecompose(PP: Particles) {
    var destloc = new SkylineArr(int, PP.r(1));
    [(x1,l1) in zip(PP.r(1), destloc)] l1 = (x1*numLocales):int;

    const plan = new ScatterPlan(destloc);
    var tup : 6*SkylineArr(real(32));

    for param idim in 1..Ndim {
      tup(idim) = PP.r(idim);
      tup(3+idim) = PP.p(idim);
    }
    plan.execute((...tup),PP.pid);

    // Repartition f
    plan.repartition(PP.f(1), PP.f(2), PP.f(3));

    delete destloc;
    delete plan;
  }

  // Initialize particles onto a grid
  proc initializeParticlesOnGrid(Nc : int, displace : 3*real(32)
                                 = (0:real(32),
                                    0:real(32),
                                    0:real(32))) : Particles {
    if (Nc%numLocales != 0) then
      halt("Choose Nc to be divisible by numLocales");
    const npart = Nc**3;
    const slab = Nc/numLocales;

    var PP = new Particles(npart);

    // We will use the fact that we know the structure of
    // the Particles record here...
    coforall loc in Locales with (const in displace) {
      on loc {
        ref xarr = PP.r(1).getLocalArray();
        ref yarr = PP.r(2).getLocalArray();
        ref zarr = PP.r(3).getLocalArray();
        ref pidarr = PP.pid.getLocalArray();
        local {
          const invNc = (1.0/Nc):real(32);
          const mypart = {slab*here.id.. #slab, 0.. #Nc, 0.. #Nc};
          // Sanity check
          if (xarr.size != mypart.size) &&
            (yarr.size != mypart.size) &&
            (zarr.size != mypart.size) {
            halt("Sizes don't match! %i %i %i %i".format(xarr.size, yarr.size, zarr.size, mypart.size));
          }
          const myfirst = mypart.dim(1).first;
          const offset = mypart.size * here.id;
          forall idx in mypart {
            const ip = ((idx(1)-myfirst)*Nc + idx(2))*Nc + idx(3);
            xarr[ip] = (idx(1):real(32) + displace(1))*invNc;
            yarr[ip] = (idx(2):real(32) + displace(2))*invNc;
            zarr[ip] = (idx(3):real(32) + displace(3))*invNc;
            pidarr[ip] = ip+offset;
          }
        }
      }
    }

    return PP;
  }


  // End module
}
