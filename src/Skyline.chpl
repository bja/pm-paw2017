module Skyline {

  config const printSkylineTimings=false;
  config const scatterWorkers=here.maxTaskPar;

  use PrivateDist;
  use ReplicatedDist;
  use Barrier;
  use Time;
  use RangeChunk;

  /* Helper POD for the scatter operations, to make
     them reproducible.
  */

  class ScatterPlan {
    const Dwork1 = {0.. #scatterWorkers, 0.. #numLocales};
    const Dwork = Dwork1 dmapped Replicated();
    const Dloc1 = {0.. #numLocales};
    const Dloc = Dloc1 dmapped Replicated();

    var allBuckets : [Dwork] int;
    var allOffsets : [Dwork] int;
    var numParticles : [Dloc] int;
    var posOffsets : [Dloc] int;
    var totalParticles : [PrivateSpace] int;

    var destLocales : SkylineArr(int);

    proc ScatterPlan(destlocs: SkylineArr(int)) {
      // Cache dest locales
      destLocales = destlocs;

      // This is a global index, used when we exchange particles
      var pindx: [PrivateSpace] atomic int;
      // Initialize
      forall i1 in pindx {
        i1.write(0);
      }

      forall p1 in destLocales.locArr {
        // Define local copies here....
        var localallBuckets : [0.. #scatterWorkers, 0.. #numLocales] int;
        var localallOffsets : [0.. #scatterWorkers, 0.. #numLocales] int;
        var localnumParticles : [0.. #numLocales] int;
        var localposOffsets : [0.. #numLocales] int;

        const np = p1.nelt;
        ref dests = p1.myElems[0.. #np];

        // Count and bucketize
        coforall iworker in 0.. #scatterWorkers {
          const myChunk = chunk(0.. #np, scatterWorkers, iworker);
          ref myDests = dests[myChunk];
          // Count number of particles in local buckets
          var bucketSize : [0.. #numLocales] int;
          for iloc in myDests do bucketSize[iloc] += 1;

          var sendOffsets : [0.. #numLocales] int = + scan bucketSize;
          // where we fill in the data in tmp
          sendOffsets += (myChunk.low-bucketSize);

          localallBuckets[iworker,..] = bucketSize[..];
          localallOffsets[iworker,..] = sendOffsets[..];
        }

        allBuckets[..,..] = localallBuckets[..,..];
        allOffsets[..,..] = localallOffsets[..,..];

        // Make sure everyone is here before starting
        // Exchange particles
        coforall iloc in here.id.. #numLocales {
          const destloc = iloc % numLocales;
          const npartall = + reduce (localallBuckets[.., destloc]);
          localnumParticles[destloc] = npartall;
          if (npartall > 0) then localposOffsets[destloc] = pindx[destloc].fetchAdd(npartall);
        }

        numParticles[..] = localnumParticles[..];
        posOffsets[..] = localposOffsets[..];
      }

      forall ndx in pindx {
        totalParticles[here.id] = ndx.read();
      }
    }


    proc execute(vecs...?n) {
      var tAlloc, tBucket, tExchange : real;

      var bar = new Barrier(numLocales);

      // Start the SIMD portion of the code
      // This is equivalent to a coforall loc in Locales do....
      forall loc in PrivateSpace with (max reduce tAlloc,
                                       max reduce tBucket,
                                       max reduce tExchange) {

        // Timer
        var tt : Timer;

        // Start by pulling the required plan data, and allocating work arrays
        tt.clear(); tt.start();
        const np = destLocales.locArr[here.id].nelt;
        const localallBuckets : [0.. #scatterWorkers, 0.. #numLocales] int = allBuckets[..,..];
        const localallOffsets : [0.. #scatterWorkers, 0.. #numLocales] int = allOffsets[..,..];
        const localnumParticles : [0.. #numLocales] int = numParticles;
        const localposOffsets : [0.. #numLocales] int = posOffsets;
        const finalNumParticles = totalParticles[here.id];
        ref dests = destLocales.getLocalArray();
        tt.stop();
        tAlloc = tt.elapsed();

        tBucket=0;
        tExchange=0;
         
        // Work out the scatter positions
        var ndxarr : [0.. #np] int;
        tt.clear(); tt.start();
        coforall iworker in 0.. #scatterWorkers {
          const myChunk = chunk(0.. #np, scatterWorkers, iworker);
          ref myArr = ndxarr[myChunk];
          ref myDests = dests[myChunk];
          // Count number of particles in local buckets
          //var bucketSize : [0.. #numLocales] int = localallBuckets[iworker,..];
          var sendOffsets : [0.. #numLocales] int = localallOffsets[iworker,..];

          // Bucketize
          for (ip,iloc) in zip(myArr,myDests) {
            ref idx = sendOffsets[iloc];
            ip = idx; 
            idx += 1;
          }
        }
        tt.stop();
        tBucket += tt.elapsed();

        // Now we loop over each vector in succession.....
        for param ivec in 1..n {

          tt.clear(); tt.start();
          var tmp : [0.. #np] vecs(ivec).eltType;
          ref locArr = vecs(ivec).locArr;
          ref p1 = locArr[here.id];
          ref arr = p1.myElems[0.. #np];

          // Bucketize
          forall (iloc, x) in zip(ndxarr, arr) do tmp[iloc] = x;
          tt.stop();
          tBucket += tt.elapsed();

          // Make sure everyone is here before starting
          tt.clear(); tt.start();
          bar.barrier();
          sync {

            // Exchange particles
            coforall iloc in here.id.. #numLocales {
              const destloc = iloc % numLocales;
              const npartall = localnumParticles[destloc];
              if (npartall > 0) {
                var pos = localposOffsets[destloc];
                for ii in 0.. #scatterWorkers {
                  const npart = localallBuckets[ii, destloc];
                  const i0 = localallOffsets[ii, destloc];
                  begin {
                    locArr[destloc].myElems[pos.. #npart] = tmp[i0.. #npart];
                  }
                  pos += npart;
                }
              }
            }
          }

          // When everything is done, reset particle counts
          tt.stop();
          tExchange += tt.elapsed();
          p1.nelt = finalNumParticles;

          // End loop over vectors
        }


      }

      if (printSkylineTimings) {
        writef("Allocations: %8.3dr (s), Bucketize: %8.3dr (s), Exchange: %8.3dr (s)\n",tAlloc, tBucket, tExchange);
      }

      // End execute function
    }

    /* Doesn't actually shuffle, but resets the number counts. Useful for scratch arrays that will
       be overwritten */
    proc repartition(vecs...?n) {
      // Start the SIMD portion of the code
      // This is equivalent to a coforall loc in Locales do....
      forall loc in PrivateSpace {
        const finalNumParticles = totalParticles[here.id];

        // Now we loop over each vector in succession.....
        for param ivec in 1..n {
          ref p1 = vecs(ivec).locArr[here.id];
          p1.nelt = finalNumParticles;
          // End loop over vectors
        }
      }
    }

    // End class 
  }


  /* Skyline array class.

     We allocate a backing array, with size a factor of overloadFactor
     larger. This allows us to reshuffle objects, or grow the actual
     number of particles.

     We provide a 2D view of this array, where the first index is the
     locale id, with the second index looking into the array. We do
     not restrict the user from accessing any part of the full backing
     array, and the user is responsible (in these cases) for updating
     the appropriate metadata.

     These classes are designed for use in forall loops, and we enable
     zippering different Skyline arrays together.
  */
  class SkylineArr {
    type eltType;

    var locArr : [PrivateSpace] LocalSkylineArr(eltType);


    /* Constructor. This builds an array that is ~uniformly distributed. */
    proc SkylineArr(type eltType, np: int, overloadFactor=2) {
      // This runs locally
      forall A in locArr {
        var mynp = np/numLocales;
        const rem = np%numLocales;
        if (here.id < rem) then mynp += 1;
        A = new LocalSkylineArr(eltType, mynp, (np/numLocales+1)*overloadFactor);
      }
    }

    /* Constructor. Copy the number of elements from each local array */
    proc SkylineArr(type eltType, other : SkylineArr(?T)) {
      // Process on individual locales
      forall A in locArr {
        const myOther = other.locArr[here.id];
        A = new LocalSkylineArr(eltType, myOther);
      }
    }

    proc deinit() {
      forall A in locArr {
        delete A;
      }
    }

    /* Return the total size of the array.

       Note that this does involve accumulating the
       number of local elements from each point.
    */
    proc size() {
      var np : int;
      forall A in locArr with (+ reduce np) do np = A.nelt;
      return np;
    }

    /* Debugging routine */
    proc displayRepresentation() {
      for A in locArr {
        on A.locale do A.displayRepresentation();
      }
    }

    /* Provide 2D access to the array.

       Note that these currently trigger nonlocal accesses, and
       so are really only provided for convenience, rather than
       for detailed use.
    */
    inline proc this(ndx : 2*int) ref {
      return locArr[ndx(1)].myElems[ndx(2)];
    }

    inline proc this(i: int, j: int) ref {
      return this((i,j));
    }

    /* Serial iterator */
    iter these() ref {
      for ii in 0.. #numLocales {
        const nelt = locArr[ii].nelt;
        for jj in 0.. #nelt {
          yield this((ii,jj));
        }
      }
    }

    /* Leader-follower iterators.

       NOTE:
       1. Currently, these are only designed to work with other Skyline
       arrays.
    */
    iter these(param tag:iterKind) where tag==iterKind.leader {
      coforall loc in Locales do
        on loc {
          const t1 = here.id..here.id;
          const locSky = locArr[here.id];
          const locDom = {0.. #locSky.nelt};
          for followThis in locDom._value.these(tag) do
            yield (t1,followThis(1));
        }
    }

    iter these(param tag:iterKind, followThis) ref
          where tag==iterKind.follower && followThis.size==2 {
      // Optimize in the case where everything is local
      ref myArr = getLocalBackingArray();
      const (r1, r2) = followThis;
      if (r1.size > 1) then halt("Unimplemented follower iteration!");
      const locid = r1.low;
      if (locid != here.id) then halt("I won't follow non-locally!"); 
      for ii in r2 do
        yield myArr[ii];
    }

    /* Return the local backing array.
    */
    proc getLocalBackingArray() ref {
      const a1 = locArr[here.id];
      return a1.myElems;
    }

    pragma "no copy return"
    proc getLocalArray() {
      const a1 = locArr[here.id];
      return a1.myElems[0.. #a1.nelt];
    }

    /* Return the local domain */
    proc getLocalDomain() {
      const a1 = locArr[here.id];
      return {0.. #a1.nelt};
    }

    // End of skyline array
  }

  class LocalSkylineArr {
    type eltType;
    var D : domain(1);
    var myElems : [D] eltType;
    var nelt : int;
    
    proc LocalSkylineArr(type eltType, nelt:int, ncapacity:int) {
      this.D = {0.. #ncapacity};
      this.nelt = nelt;
    }

    proc LocalSkylineArr(type eltType, other : LocalSkylineArr(?T)) {
      this.D = other.D;
      this.nelt = other.nelt;
    }

    /* Debugging routine */
    proc displayRepresentation() {
      writeln(here.id, " owns domain ", D, " and #elements=", nelt);
    }
  }

  /* Another iterator that loops over the local arrays, instead of
     the individual elements.

     Using the serial iterator throws a runtime error.
  */
  iter localArrays(A : SkylineArr(?T)) ref {
    // Throws a runtime error
    halt("Serial iterator not implemented for this case");
  }

  iter localArrays(param tag:iterKind, A : SkylineArr(?T)) ref
    where tag==iterKind.leader {
    coforall loc in Locales do
      on loc {
        yield here.id;
      }
  }

  iter localArrays(param tag:iterKind, A : SkylineArr(?T), followThis) ref
    where tag==iterKind.follower {
    if here.id != followThis then
      halt("Unexpected followThis in localArrays");
    const a1 = A.locArr[here.id];
    yield a1.myElems[0.. #a1.nelt];
  }


  // End module
}
