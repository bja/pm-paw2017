/*
 * Copyright 2004-2017 Cray Inc.
 * Other additional copyright holders may be indicated within.
 * 
 * The entirety of this work is licensed under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * 
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

//
// The FFTW3D distribution is defined with six classes:
//
//   FFTW3D       : distribution class
//   FFTW3DDom    : domain class
//   FFTW3DArr    : array class
//   LocFFTW3D    : local distribution class (per-locale instances)
//   LocFFTW3DDom : local domain class (per-locale instances)
//   LocFFTW3DArr : local array class (per-locale instances)
//
// When a distribution, domain, or array class instance is created, a
// corresponding local class instance is created on each locale that is
// mapped to by the distribution.
//

//
// TO DO List
//
// 1. refactor pid fields from distribution, domain, and array classes
//

use MPI;
use C_FFTW_MPI;
use Time;

use DSIUtil;
use ChapelUtil;
use CommDiagnostics;
use SparseBlockDist;
use LayoutCSR;
//
// These flags are used to output debug information and run extra
// checks when using FFTW3D.  Should these be promoted so that they can
// be used across all distributions?  This can be done by turning them
// into compiler flags or adding config parameters to the internal
// modules, perhaps called debugDists and checkDists.
//
config param debugFFTW3DDist = false;
config param debugFFTW3DDistBulkTransfer = false;

// This flag is used to enable bulk transfer when aliased arrays are
// involved.  Currently, aliased arrays are not eligible for the
// optimization due to a bug in bulk transfer for rank changed arrays
// in which the last (right-most) dimension is collapsed.  Disabling
// the optimization for all aliased arrays is very conservative, so
// we provide this flag to allow the user to override the decision,
// with the caveat that it will likely not work for the above case.
config const disableAliasedBulkTransfer = true;

config param sanityCheckDistribution = false;

//
// If the testFastFollowerOptimization flag is set to true, the
// follower will write output to indicate whether the fast follower is
// used or not.  This is used in regression testing to ensure that the
// 'fast follower' optimization is working.
//
config param testFastFollowerOptimization = false;

//
// This flag is used to disable lazy initialization of the RAD cache.
//
config param disableFFTW3DLazyRAD = defaultDisableLazyRADOpt;

// Setup FFTW Planner flag
config const useFFTPlanner = Planner.Measure;
const fftwPlanner=translateFFTWPlanner(useFFTPlanner);

//
// FFTW3D Distribution Class
//
//   The fields dataParTasksPerLocale, dataParIgnoreRunningTasks, and
//   dataParMinGranularity can be changed, but changes are
//   not reflected in privatized copies of this distribution.  Perhaps
//   this is a feature, not a bug?!
//
// rank : generic rank that must match the rank of domains and arrays
//        declared over this distribution
//
// idxType: generic index type that must match the index type of
//          domains and arrays declared over this distribution
//
// boundingBox: a non-distributed domain defining a bounding box used
//              to partition the space of all indices across the array
//              of target locales; the indices inside the bounding box
//              are partitioned "evenly" across the locales and
//              indices outside the bounding box are mapped to the
//              same locale as the nearest index inside the bounding
//              box
//
// targetLocDom: a non-distributed domain over which the array of
//               target locales and the array of local distribution
//               classes are defined
//
// targetLocales: a non-distributed array containing the target
//                locales to which this distribution partitions indices
//                and data
//
// locDist: a non-distributed array of local distribution classes
//
// dataParTasksPerLocale: an integer that specifies the number of tasks to
//                        use on each locale when iterating in parallel over
//                        a FFTW3D-distributed domain or array
//
// dataParIgnoreRunningTasks: a boolean what dictates whether the number of
//                            task use on each locale should be limited
//                            by the available parallelism
//
// dataParMinGranularity: the minimum required number of elements per
//                        task created
//

// chpldoc TODO:
//   a good reference to
//     dataParTasksPerLocale, dataParIgnoreRunningTasks, dataParMinGranularity
//   remove the above comments to avoid duplication/maintenance?
//   talk about RAD - here or in DefaultRectangular?
//   supports RAD opt, Bulk Transfer optimization, localSubdomain
//   disableFFTW3DLazyRAD
//   disableAliasedBulkTransfer
//
/*
This FFTW3D distribution partitions indices into blocks
according to a ``boundingBox`` domain
and maps each entire block onto a locale from a ``targetLocales`` array.

The indices inside the bounding box are partitioned "evenly" across
the target locales. An index outside the bounding box is mapped to the
same locale as the nearest index inside the bounding box.

Formally, an index ``idx`` is mapped to ``targetLocales[locIdx]``,
where ``locIdx`` is computed as follows.

In the 1-dimensional case, for a FFTW3D distribution with:


  =================   ====================
  ``boundingBox``     ``{low..high}``
  ``targetLocales``   ``[0..N-1] locale``
  =================   ====================

we have:

  ===================    ==========================================
  if ``idx`` is ...      ``locIdx`` is ...
  ===================    ==========================================
  ``low<=idx<=high``     ``floor(  (idx-low)*N / (high-low+1)  )``
  ``idx < low``          ``0``
  ``idx > high``         ``N-1``
  ===================    ==========================================

In the multidimensional case, ``idx`` and ``locIdx`` are tuples
of indices; ``boundingBox`` and ``targetLocales`` are multi-dimensional;
the above computation is applied in each dimension.


**Example**

The following code declares a domain ``D`` distributed over
a FFTW3D distribution with a bounding box equal to the domain ``Space``,
and declares an array ``A`` over that domain.
The `forall` loop sets each array element
to the ID of the locale to which it is mapped.

  .. code-block:: chapel

    use FFTW3DDist;

    const Space = {1..8, 1..8};
    const D: domain(2) dmapped FFTW3D(boundingBox=Space) = Space;
    var A: [D] int;

    forall a in A do
      a = a.locale.id;

    writeln(A);

When run on 6 locales, the output is:

  ::

    0 0 0 0 1 1 1 1
    0 0 0 0 1 1 1 1
    0 0 0 0 1 1 1 1
    2 2 2 2 3 3 3 3
    2 2 2 2 3 3 3 3
    2 2 2 2 3 3 3 3
    4 4 4 4 5 5 5 5
    4 4 4 4 5 5 5 5


**Constructor Arguments**

The ``FFTW3D`` class constructor is defined as follows:

  .. code-block:: chapel

    proc FFTW3D(
      boundingBox: domain,
      targetLocales: [] locale  = Locales, 
      dataParTasksPerLocale     = // value of  dataParTasksPerLocale      config const,
      dataParIgnoreRunningTasks = // value of  dataParIgnoreRunningTasks  config const,
      dataParMinGranularity     = // value of  dataParMinGranularity      config const,
      param rank                = boundingBox.rank,
      type  idxType             = boundingBox.idxType,
      type  sparseLayoutType    = DefaultDist)

The arguments ``boundingBox`` (a domain) and ``targetLocales`` (an array)
define the mapping of any index of ``idxType`` type to a locale
as described above.

The rank of ``targetLocales`` must match the rank of the distribution,
or be ``1``.  If the rank of ``targetLocales`` is ``1``, a greedy
heuristic is used to reshape the array of target locales so that it
matches the rank of the distribution and each dimension contains an
approximately equal number of indices.

The arguments ``dataParTasksPerLocale``, ``dataParIgnoreRunningTasks``,
and ``dataParMinGranularity`` set the knobs that are used to
control intra-locale data parallelism for FFTW3D-distributed domains
and arrays in the same way that the like-named config constants
control data parallelism for ranges and default-distributed domains
and arrays.

The ``rank`` and ``idxType`` arguments are inferred from the ``boundingBox``
argument unless explicitly set.  They must match the rank and index type of the
domains "dmapped" using that FFTW3D instance. If the ``boundingBox`` argument is
a stridable domain, the stride information will be ignored and the
``boundingBox`` will only use the lo..hi bounds.

When a ``sparse subdomain`` is created for a ``FFTW3D`` distributed domain, the
``sparseLayoutType`` will be the layout of these sparse domains. The default is
currently coordinate, but :class:`LayoutCSR.CSR` is an interesting alternative.

**Data-Parallel Iteration**

A `forall` loop over a FFTW3D-distributed domain or array
executes each iteration on the locale where that iteration's index
is mapped to.

Parallelism within each locale is guided by the values of
``dataParTasksPerLocale``, ``dataParIgnoreRunningTasks``, and
``dataParMinGranularity`` of the respective FFTW3D instance.
Updates to these values, if any, take effect only on the locale
where the updates are made.

**Sparse Subdomains**

When a ``sparse subdomain`` is declared as a subdomain to a FFTW3D-distributed
domain, the resulting sparse domain will also be FFTW3D-distributed. The
sparse layout used in this sparse subdomain can be controlled with the
``sparseLayoutType`` constructor argument to FFTW3D.

This example demonstrates a FFTW3D-distributed sparse domain and array:

  .. code-block:: chapel

   use FFTW3DDist;

    const Space = {1..8, 1..8};

    // Declare a dense, FFTW3D-distributed domain.
    const DenseDom: domain(2) dmapped FFTW3D(boundingBox=Space) = Space;

    // Declare a sparse subdomain.
    // Since DenseDom is FFTW3D-distributed, SparseDom will be as well.
    var SparseDom: sparse subdomain(DenseDom);

    // Add some elements to the sparse subdomain.
    // SparseDom.bulkAdd is another way to do this that allows more control.
    SparseDom += [ (1,2), (3,6), (5,4), (7,8) ];

    // Declare a sparse array.
    // This array is also FFTW3D-distributed.
    var A: [SparseDom] int;

    A = 1;

    writeln( "A[(1, 1)] = ", A[1,1]);
    for (ij,x) in zip(SparseDom, A) {
      writeln( "A[", ij, "] = ", x, " on locale ", x.locale);
    }

   // Results in this output when run on 4 locales:
   // A[(1, 1)] = 0
   // A[(1, 2)] = 1 on locale LOCALE0
   // A[(3, 6)] = 1 on locale LOCALE1
   // A[(5, 4)] = 1 on locale LOCALE2
   // A[(7, 8)] = 1 on locale LOCALE3


*/
class FFTW3D : BaseDist {
  param rank: int;
  type idxType = int;
  var boundingBox: domain(rank, idxType);
  var targetLocDom: domain(rank);
  var targetLocales: [targetLocDom] locale;
  var locDist: [targetLocDom] LocFFTW3D(rank, idxType);
  var dataParTasksPerLocale: int;
  var dataParIgnoreRunningTasks: bool;
  var dataParMinGranularity: int;
  type sparseLayoutType = DefaultDist;

  var Ng : int;
  var slabSize : int;
  var nghosts : int;
}

//
// Local FFTW3D Distribution Class
//
// rank : generic rank that matches FFTW3D.rank
// idxType: generic index type that matches FFTW3D.idxType
// myChunk: a non-distributed domain that defines this locale's indices
//
class LocFFTW3D {
  param rank: int;
  type idxType;
  const myChunk: domain(rank, idxType);
}

//
// FFTW3D Domain Class
//
// rank:      generic domain rank
// idxType:   generic domain index type
// stridable: generic domain stridable parameter
// dist:      reference to distribution class
// locDoms:   a non-distributed array of local domain classes
// whole:     a non-distributed domain that defines the domain's indices
//
class FFTW3DDom: BaseRectangularDom {
  //param rank: int;
  //type idxType;
  //param stridable: bool;
  type sparseLayoutType;
  const dist: FFTW3D(rank, idxType, sparseLayoutType);
  var locDoms: [dist.targetLocDom] LocFFTW3DDom(rank, idxType, stridable);
  var whole: domain(rank=rank, idxType=idxType, stridable=stridable);
}

//
// Local FFTW3D Domain Class
//
// rank: generic domain rank
// idxType: generic domain index type
// stridable: generic domain stridable parameter
// myBlock: a non-distributed domain that defines the local indices
//
class LocFFTW3DDom {
  param rank: int;
  type idxType;
  param stridable: bool;
  var myBlock: domain(rank, idxType, stridable);
  var myGhostedBlock: domain(rank, idxType, stridable);
}

//
// FFTW3D Array Class
//
// eltType: generic array element type
// rank: generic array rank
// idxType: generic array index type
// stridable: generic array stridable parameter
// dom: reference to domain class
// locArr: a non-distributed array of local array classes
// myLocArr: optimized reference to here's local array class (or nil)
//
class FFTW3DArr: BaseRectangularArr {
  //type eltType;
  //param rank: int;
  //type idxType;
  //param stridable: bool;
  type sparseLayoutType;
  var doRADOpt: bool = defaultDoRADOpt;
  var dom: FFTW3DDom(rank, idxType, stridable, sparseLayoutType);
  var locArr: [dom.dist.targetLocDom] LocFFTW3DArr(eltType, rank, idxType, stridable);
  pragma "local field"
  var myLocArr: LocFFTW3DArr(eltType, rank, idxType, stridable);
  const SENTINEL = max(rank*idxType);
}

//
// Local FFTW3D Array Class
//
// eltType: generic array element type
// rank: generic array rank
// idxType: generic array index type
// stridable: generic array stridable parameter
// locDom: reference to local domain class
// myElems: a non-distributed array of local elements
//
class LocFFTW3DArr {
  type eltType;
  param rank: int;
  type idxType;
  param stridable: bool;
  const locDom: LocFFTW3DDom(rank, idxType, stridable);
  var locRAD: LocRADCache(eltType, rank, idxType, stridable); // non-nil if doRADOpt=true
  pragma "local field"
  var myElems: [locDom.myGhostedBlock] eltType; 
  var locRADLock: atomicbool; // This will only be accessed locally
                              // force the use of processor atomics

  var fwd, rev: fftw_plan;
  var fwdt, revt: fftw_plan; // Support the transpose optimization

  // These functions will always be called on this.locale, and so we do
  // not have an on statement around the while loop below (to avoid
  // the repeated on's from calling testAndSet()).
  inline proc lockLocRAD() {
    while locRADLock.testAndSet() do chpl_task_yield();
  }

  inline proc unlockLocRAD() {
    locRADLock.clear();
  }
}

//
// FFTW3D constructor for clients of the FFTW3D distribution
//
proc FFTW3D.FFTW3D(Ng: int,
                   nghosts: int = 0,
                   dataParTasksPerLocale=getDataParTasksPerLocale(),
                   dataParIgnoreRunningTasks=getDataParIgnoreRunningTasks(),
                   dataParMinGranularity=getDataParMinGranularity(),
                   param rank = 3,
                   type idxType = int,
                   type sparseLayoutType = DefaultDist) {
  // This is all specific to FFTW3D
  if rank != 3 then 
    compilerError("FFTW3D is only supported for 3-dimensional domains");

  // Verify Ng meets all requirements
  if (Ng%2 != 0) then
    halt("FFTW3D requires that Ng be divisible by 2");
  if (Ng%numLocales != 0) then
    halt("FFTW3D requires that Ng be divisible by numLocales");
  this.Ng = Ng;
  this.slabSize = Ng/numLocales;
  this.nghosts = nghosts;

  if (this.slabSize < this.nghosts) then
    halt("slabSize < nghosts. FFTW3D requires that the ghosts only come from left and right.");


  // Bits from the Block constructor
  var boundingBox = {0.. #Ng, 0.. #Ng, 0.. #(Ng+2)};
  var targetLocales: [0..#numLocales,0..0,0..0] locale;
  targetLocales[..,0,0]=Locales;

  if rank != boundingBox.rank then
    compilerError("specified FFTW3D rank != rank of specified bounding box");
  if idxType != boundingBox.idxType then
    compilerError("specified FFTW3D index type != index type of specified bounding box");
  if rank != 2 && sparseLayoutType == CSR then 
    compilerError("CSR layout is only supported for 2 dimensional domains");

  if boundingBox.size == 0 then
    halt("FFTW3D() requires a non-empty boundingBox");

  // Original constructor goes here
  this.boundingBox = boundingBox : domain(rank, idxType, stridable = false);

  setupTargetLocalesArray(targetLocDom, this.targetLocales, targetLocales);

  const boundingBoxDims = this.boundingBox.dims();
  const targetLocDomDims = targetLocDom.dims();
  coforall locid in targetLocDom do
    on this.targetLocales(locid) do
      locDist(locid) =  new LocFFTW3D(rank, idxType, locid, boundingBoxDims,
                                      targetLocDomDims);

  // NOTE: When these knobs stop using the global defaults, we will need
  // to add checks to make sure dataParTasksPerLocale<0 and
  // dataParMinGranularity<0
  this.dataParTasksPerLocale = if dataParTasksPerLocale==0
    then here.maxTaskPar
      else dataParTasksPerLocale;
  this.dataParIgnoreRunningTasks = dataParIgnoreRunningTasks;
  this.dataParMinGranularity = dataParMinGranularity;

  if debugFFTW3DDist {
    writeln("Creating new FFTW3D distribution:");
    dsiDisplayRepresentation();
  }
}

proc FFTW3D.dsiAssign(other: this.type) {
  compilerError("dsiAssign not implemented for FFTWDist");
  /*
  coforall locid in targetLocDom do
    on targetLocales(locid) do
      delete locDist(locid);
  boundingBox = other.boundingBox;
  targetLocDom = other.targetLocDom;
  targetLocales = other.targetLocales;
  dataParTasksPerLocale = other.dataParTasksPerLocale;
  dataParIgnoreRunningTasks = other.dataParIgnoreRunningTasks;
  dataParMinGranularity = other.dataParMinGranularity;
  const boundingBoxDims = boundingBox.dims();
  const targetLocDomDims = targetLocDom.dims();

  coforall locid in targetLocDom do
    on targetLocales(locid) do
      locDist(locid) = new LocFFTW3D(rank, idxType, locid, boundingBoxDims,
                                    targetLocDomDims);
  */
}

//
// FFTW3D distributions are equivalent if they share the same bounding
// box and target locale set.
//
proc FFTW3D.dsiEqualDMaps(that: FFTW3D(?)) {
  return (this.boundingBox == that.boundingBox &&
          this.targetLocales.equals(that.targetLocales));
}

//
// FFTW3D distributions are not equivalent to other domain maps.
//
proc FFTW3D.dsiEqualDMaps(that) param {
  return false;
}

proc FFTW3D.dsiClone() {
  return new FFTW3D(Ng, nghosts,
                   dataParTasksPerLocale, dataParIgnoreRunningTasks,
                   dataParMinGranularity,
                   rank,
                   idxType,
                   sparseLayoutType);
}

proc FFTW3D.dsiDestroyDist() {
  coforall ld in locDist do {
    on ld do
      delete ld;
  }
}

proc FFTW3D.dsiDisplayRepresentation() {
  writeln("boundingBox = ", boundingBox);
  writeln("ghosts = ",nghosts);
  writeln("targetLocDom = ", targetLocDom);
  writeln("targetLocales = ", for tl in targetLocales do tl.id);
  writeln("dataParTasksPerLocale = ", dataParTasksPerLocale);
  writeln("dataParIgnoreRunningTasks = ", dataParIgnoreRunningTasks);
  writeln("dataParMinGranularity = ", dataParMinGranularity);
  for tli in targetLocDom do
    writeln("locDist[", tli, "].myChunk = ", locDist[tli].myChunk);
}

proc FFTW3D.dsiNewRectangularDom(param rank: int, type idxType,
                                param stridable: bool, inds) {
  if idxType != this.idxType then
    compilerError("FFTW3D domain index type does not match distribution's");
  if rank != this.rank then
    compilerError("FFTW3D domain rank does not match distribution's");

  var dom = new FFTW3DDom(rank=rank, idxType=idxType, dist=this,
      stridable=stridable, sparseLayoutType=sparseLayoutType);
  dom.dsiSetIndices(inds);
  if debugFFTW3DDist {
    writeln("Creating new FFTW3D domain:");
    dom.dsiDisplayRepresentation();
  }
  return dom;
}

proc FFTW3D.dsiNewSparseDom(param rank: int, type idxType, dom: domain) {
  compilerError("Sparse domains not supported for FFTW");
  /*
  return new SparseBlockDom(rank=rank, idxType=idxType,
      sparseLayoutType=sparseLayoutType, dist=this, whole=dom._value.whole, 
      parentDom=dom);
  */
}

//
// output distribution
//
proc FFTW3D.writeThis(x) {
  x.writeln("FFTW3D");
  x.writeln("-------");
  x.writeln("distributes: ", boundingBox);
  x.writeln("across locales: ", targetLocales);
  x.writeln("indexed via: ", targetLocDom);
  x.writeln("resulting in: ");
  for locid in targetLocDom do
    x.writeln("  [", locid, "] locale ", locDist(locid).locale.id, " owns chunk: ", locDist(locid).myChunk);
}

proc FFTW3D.dsiIndexToLocale(ind: idxType) where rank == 1 {
  return targetLocales(targetLocsIdx(ind));
}

proc FFTW3D.dsiIndexToLocale(ind: rank*idxType) {
  return targetLocales(targetLocsIdx(ind));
}

//
// compute what chunk of inds is owned by a given locale -- assumes
// it's being called on the locale in question
//
proc FFTW3D.getChunk(inds, locid) {
  // use domain slicing to get the intersection between what the
  // locale owns and the domain's index set
  //
  // TODO: Should this be able to be written as myChunk[inds] ???
  //
  // TODO: Does using David's detupling trick work here?
  //
  const chunk = locDist(locid).myChunk((...inds.getIndices()));
  if sanityCheckDistribution then
    if chunk.numIndices > 0 {
      if targetLocsIdx(chunk.low) != locid then
        writeln("[", here.id, "] ", chunk.low, " is in my chunk but maps to ",
                targetLocsIdx(chunk.low));
      if targetLocsIdx(chunk.high) != locid then
        writeln("[", here.id, "] ", chunk.high, " is in my chunk but maps to ",
                targetLocsIdx(chunk.high));
    }
  return chunk;
}

//
// get the index into the targetLocales array for a given distributed index
//
proc FFTW3D.targetLocsIdx(ind: idxType) where rank == 1 {
  return targetLocsIdx((ind,));
}

proc FFTW3D.targetLocsIdx(ind: rank*idxType) {
  var result: rank*int;
  for param i in 1..rank do
    result(i) = max(0, min((targetLocDom.dim(i).length-1):int,
                           (((ind(i) - boundingBox.dim(i).low) *
                             targetLocDom.dim(i).length:idxType) /
                            boundingBox.dim(i).length):int));
  return if rank == 1 then result(1) else result;
}

proc FFTW3D.dsiCreateReindexDist(newSpace, oldSpace) {
  compilerError("dsiCreateReindexDist not supported by FFTW");
  /*
  proc anyStridable(space, param i=1) param
    return if i == space.size then space(i).stridable
           else space(i).stridable || anyStridable(space, i+1);

  // Should this error be in ChapelArray or not an error at all?
  if newSpace(1).idxType != oldSpace(1).idxType then
    compilerError("index type of reindex domain must match that of original domain");
  if anyStridable(newSpace) || anyStridable(oldSpace) then
    compilerWarning("reindexing stridable FFTW3D arrays is not yet fully supported");

  /* To shift the bounding box, we must perform the following operation for
   *  each dimension:
   *
   *   bbox(r).low - (oldSpace(r).low - newSpace(r).low)
   *   bbox(r).high - (oldSpace(r).low - newSpace(r).low)
   *
   * The following is guaranteed on entry:
   *
   *   oldSpace(r).low-newSpace(r).low = oldSpace(r).high-newSpace(r).high
   *
   * We need to be able to do this without going out of range of the index
   *  type.  The approach we take is to check if there is a way to perform
   *  the calculation without having any of the intermediate results go out
   *  of range.
   *
   *    newBbLow = bbLow - (oldLow - newLow)
   *    newBbLow = bbLow - oldLow + newLow
   *
   * Can be performed as:
   *
   *    t = oldLow-newLow;
   *    newBbLow = bbLow-t;
   * or
   *    t = bbLow-oldLow;
   *    newBbLow = t+newLow;
   * or
   *    t = bbLow+newLow;
   *    newBbLow = t-oldLow;
   *
   */
  proc adjustBound(bbound, oldBound, newBound) {
    var t: bbound.type;
    if safeSub(oldBound, newBound) {
      t = oldBound-newBound;
      if safeSub(bbound, t) {
        return (bbound-t, true);
      }
    }
    if safeSub(bbound, oldBound) {
      t = bbound-oldBound;
      if safeAdd(t, newBound) {
        return (t+newBound, true);
      }
    }
    if safeAdd(bbound, newBound) {
      t = bbound+newBound;
      if safeSub(t, oldBound) {
        return(t-oldBound, true);
      }
    }
    return (bbound, false);
  }

  var myNewBbox = boundingBox.dims();
  for param r in 1..rank {
    var oldLow = oldSpace(r).low;
    var newLow = newSpace(r).low;
    var oldHigh = oldSpace(r).high;
    var newHigh = newSpace(r).high;
    var valid: bool;
    if oldLow != newLow {
      (myNewBbox(r)._low,valid) = adjustBound(myNewBbox(r).low,oldLow,newLow);
      if !valid then // try with high
        (myNewBbox(r)._low,valid) = adjustBound(myNewBbox(r).low,oldHigh,newHigh);
      if !valid then
        halt("invalid reindex for FFTW3D: distribution bounding box (low) out of range in dimension ", r);

      (myNewBbox(r)._high,valid) = adjustBound(myNewBbox(r).high,oldHigh,newHigh);
      if !valid then
        (myNewBbox(r)._high,valid) = adjustBound(myNewBbox(r).high,oldLow,newLow);
      if !valid then // try with low
        halt("invalid reindex for FFTW3D: distribution bounding box (high) out of range in dimension ", r);
    }
  }
  var d = {(...myNewBbox)};
  var newDist = new FFTW3D(d, targetLocales, 
                          dataParTasksPerLocale, dataParIgnoreRunningTasks,
                          dataParMinGranularity);
  return newDist;
  */
}


proc LocFFTW3D.LocFFTW3D(param rank: int,
                      type idxType, 
                      locid, // the locale index from the target domain
                      boundingBox: rank*range(idxType),
                      targetLocBox: rank*range) {
  if rank == 1 {
    const lo = boundingBox(1).low;
    const hi = boundingBox(1).high;
    const numelems = hi - lo + 1;
    const numlocs = targetLocBox(1).length;
    const (blo, bhi) = _computeBlock(numelems, numlocs, locid,
                                     max(idxType), min(idxType), lo);
    myChunk = {blo..bhi};
  } else {
    var inds: rank*range(idxType);
    for param i in 1..rank {
      const lo = boundingBox(i).low;
      const hi = boundingBox(i).high;
      const numelems = hi - lo + 1;
      const numlocs = targetLocBox(i).length;
      const (blo, bhi) = _computeBlock(numelems, numlocs, locid(i),
                                       max(idxType), min(idxType), lo);
      inds(i) = blo..bhi;
    }
    myChunk = {(...inds)};
  }
}


proc FFTW3DDom.dsiMyDist() return dist;

proc FFTW3DDom.dsiDisplayRepresentation() {
  writeln("whole = ", whole);
  for tli in dist.targetLocDom do
    writeln("locDoms[", tli, "].myBlock = ", locDoms[tli].myBlock);
}

proc FFTW3DDom.dsiDims() return whole.dims();

proc FFTW3DDom.dsiDim(d: int) return whole.dim(d);

// stopgap to avoid accessing locDoms field (and returning an array)
proc FFTW3DDom.getLocDom(localeIdx) return locDoms(localeIdx);


//
// Given a tuple of scalars of type t or range(t) match the shape but
// using types rangeType and scalarType e.g. the call:
// _matchArgsShape(range(int(32)), int(32), (1:int(64), 1:int(64)..5, 1:int(64)..5))
// returns the type: (int(32), range(int(32)), range(int(32)))
//
proc _matchArgsShape(type rangeType, type scalarType, args) type {
  proc helper(param i: int) type {
    if i == args.size {
      if isCollapsedDimension(args(i)) then
        return (scalarType,);
      else
        return (rangeType,);
    } else {
      if isCollapsedDimension(args(i)) then
        return (scalarType, (... helper(i+1)));
      else
        return (rangeType, (... helper(i+1)));
    }
  }
  return helper(1);
}


iter FFTW3DDom.these() {
  for i in whole do
    yield i;
}

iter FFTW3DDom.these(param tag: iterKind) where tag == iterKind.leader {
  const maxTasks = dist.dataParTasksPerLocale;
  const ignoreRunning = dist.dataParIgnoreRunningTasks;
  const minSize = dist.dataParMinGranularity;
  const wholeLow = whole.low;

  // If this is the only task running on this locale, we don't want to
  // count it when we try to determine how many tasks to use.  Here we
  // check if we are the only one running, and if so, use
  // ignoreRunning=true for this locale only.  Obviously there's a bit
  // of a race condition if some other task starts after we check, but
  // in that case there is no correct answer anyways.
  //
  // Note that this code assumes that any locale will only be in the
  // targetLocales array once.  If this is not the case, then the
  // tasks on this locale will *all* ignoreRunning, which may have
  // performance implications.
  const hereId = here.id;
  const hereIgnoreRunning = if here.runningTasks() == 1 then true
                            else ignoreRunning;
  coforall locDom in locDoms do on locDom {
    const myIgnoreRunning = if here.id == hereId then hereIgnoreRunning
      else ignoreRunning;
    // Use the internal function for untranslate to avoid having to do
    // extra work to negate the offset
    type strType = chpl__signedType(idxType);
    const tmpBlock = locDom.myBlock.chpl__unTranslate(wholeLow);
    var locOffset: rank*idxType;
    for param i in 1..tmpBlock.rank {
      const stride = tmpBlock.dim(i).stride;
      if stride < 0 && strType != idxType then
        halt("negative stride not supported with unsigned idxType");
        // (since locOffset is unsigned in that case)
      locOffset(i) = tmpBlock.dim(i).first / stride:idxType;
    }
    // Forward to defaultRectangular
    for followThis in tmpBlock._value.these(iterKind.leader, maxTasks,
                                            myIgnoreRunning, minSize,
                                            locOffset) do
      yield followThis;
  }
}

//
// TODO: Abstract the addition of low into a function?
// Note relationship between this operation and the
// order/position functions -- any chance for creating similar
// support? (esp. given how frequent this seems likely to be?)
//
// TODO: Is there some clever way to invoke the leader/follower
// iterator on the local blocks in here such that the per-core
// parallelism is expressed at that level?  Seems like a nice
// natural composition and might help with my fears about how
// stencil communication will be done on a per-locale basis.
//
// TODO: Can we just re-use the DefaultRectangularDom follower here?
//
iter FFTW3DDom.these(param tag: iterKind, followThis) where tag == iterKind.follower {
  proc anyStridable(rangeTuple, param i: int = 1) param
      return if i == rangeTuple.size then rangeTuple(i).stridable
             else rangeTuple(i).stridable || anyStridable(rangeTuple, i+1);

  if chpl__testParFlag then
    chpl__testParWriteln("FFTW3D domain follower invoked on ", followThis);

  var t: rank*range(idxType, stridable=stridable||anyStridable(followThis));
  type strType = chpl__signedType(idxType);
  for param i in 1..rank {
    var stride = whole.dim(i).stride: strType;
    // not checking here whether the new low and high fit into idxType
    var low = (stride * followThis(i).low:strType):idxType;
    var high = (stride * followThis(i).high:strType):idxType;
    t(i) = ((low..high by stride:strType) + whole.dim(i).alignedLow by followThis(i).stride:strType).safeCast(t(i).type);
  }
  for i in {(...t)} {
    yield i;
  }
}

//
// output domain
//
proc FFTW3DDom.dsiSerialWrite(x) {
  x.write(whole);
}

//
// how to allocate a new array over this domain
//
proc FFTW3DDom.dsiBuildArray(type eltType) {
  var arr = new FFTW3DArr(eltType=eltType, rank=rank, idxType=idxType,
      stridable=stridable, sparseLayoutType=sparseLayoutType, dom=this);
  arr.setup();
  return arr;
}

proc FFTW3DDom.dsiNumIndices return whole.numIndices;
proc FFTW3DDom.dsiLow return whole.low;
proc FFTW3DDom.dsiHigh return whole.high;
proc FFTW3DDom.dsiStride return whole.stride;
proc FFTW3DDom.dsiAlignedLow return whole.alignedLow;
proc FFTW3DDom.dsiAlignedHigh return whole.alignedHigh;
proc FFTW3DDom.dsiAlignment return whole.alignment;

//
// INTERFACE NOTES: Could we make dsiSetIndices() for a rectangular
// domain take a domain rather than something else?
//
proc FFTW3DDom.dsiSetIndices(x: domain) {
  if x.rank != rank then
    compilerError("rank mismatch in domain assignment");
  if x._value.idxType != idxType then
    compilerError("index type mismatch in domain assignment");
  whole = x;
  setup();
  if debugFFTW3DDist {
    writeln("Setting indices of FFTW3D domain:");
    dsiDisplayRepresentation();
  }
}

proc FFTW3DDom.dsiSetIndices(x) {
  if x.size != rank then
    compilerError("rank mismatch in domain assignment");
  if x(1).idxType != idxType then
    compilerError("index type mismatch in domain assignment");
  //
  // TODO: This seems weird:
  //
  whole.setIndices(x);
  setup();
  if debugFFTW3DDist {
    writeln("Setting indices of FFTW3D domain:");
    dsiDisplayRepresentation();
  }
}

proc FFTW3DDom.dsiGetIndices() {
  return whole.getIndices();
}

// dsiLocalSlice
proc FFTW3DDom.dsiLocalSlice(param stridable: bool, ranges) {
  return whole((...ranges));
}

proc FFTW3DDom.setup() {
  if locDoms(dist.targetLocDom.low) == nil {
    coforall localeIdx in dist.targetLocDom do {
      on dist.targetLocales(localeIdx) {
        locDoms(localeIdx) = new LocFFTW3DDom(rank, idxType, stridable,
                                              dist.getChunk(whole, localeIdx));
        locDoms(localeIdx).myGhostedBlock = addGhostsToDomain(locDoms(localeIdx).myBlock, dist.nghosts);
      }
    }
  } else {
    coforall localeIdx in dist.targetLocDom do {
      on dist.targetLocales(localeIdx) {
        locDoms(localeIdx).myBlock = dist.getChunk(whole, localeIdx);
        locDoms(localeIdx).myGhostedBlock = addGhostsToDomain(locDoms(localeIdx).myBlock, dist.nghosts);
      }
    }
  }
}

proc FFTW3DDom.dsiDestroyDom() {
  coforall localeIdx in dist.targetLocDom do {
    on locDoms(localeIdx) do
      delete locDoms(localeIdx);
  }
}

proc FFTW3DDom.dsiMember(i) {
  return whole.member(i);
}

proc FFTW3DDom.dsiIndexOrder(i) {
  return whole.indexOrder(i);
}

proc FFTW3DDom.dsiAssignDomain(rhs: domain, lhsPrivate:bool) {
  chpl_assignDomainWithGetSetIndices(this, rhs);
}

//
// Added as a performance stopgap to avoid returning a domain
//
proc LocFFTW3DDom.member(i) return myGhostedBlock.member(i);

proc FFTW3DArr.dsiDisplayRepresentation() {
  for tli in dom.dist.targetLocDom {
    writeln("locArr[", tli, "].myElems = ", for e in locArr[tli].myElems do e);
    if doRADOpt then
      writeln("locArr[", tli, "].locRAD = ", locArr[tli].locRAD.RAD);
  }
}

proc FFTW3DArr.dsiGetBaseDom() return dom;

//
// NOTE: Each locale's myElems array must be initialized prior to
// setting up the RAD cache.
//
proc FFTW3DArr.setupRADOpt() {
  for localeIdx in dom.dist.targetLocDom {
    on dom.dist.targetLocales(localeIdx) {
      const myLocArr = locArr(localeIdx);
      if myLocArr.locRAD != nil {
        delete myLocArr.locRAD;
        myLocArr.locRAD = nil;
      }
      if disableFFTW3DLazyRAD {
        myLocArr.locRAD = new LocRADCache(eltType, rank, idxType, stridable, dom.dist.targetLocDom);
        for l in dom.dist.targetLocDom {
          if l != localeIdx {
            myLocArr.locRAD.RAD(l) = locArr(l).myElems._value.dsiGetRAD();
          }
        }
      }
    }
  }
}

proc FFTW3DArr.setup() {
  var thisid = this.locale.id;
  coforall localeIdx in dom.dist.targetLocDom {
    on dom.dist.targetLocales(localeIdx) {
      const locDom = dom.getLocDom(localeIdx);
      locArr(localeIdx) = new LocFFTW3DArr(eltType, rank, idxType, stridable, locDom);
      locArr(localeIdx).setup();
      if thisid == here.id then
        myLocArr = locArr(localeIdx);
    }
  }

  if doRADOpt && disableFFTW3DLazyRAD then setupRADOpt();
}

proc FFTW3DArr.dsiDestroyArr(isslice:bool) {
  coforall localeIdx in dom.dist.targetLocDom {
    on locArr(localeIdx) {
      ref arr = locArr(localeIdx);
      Barrier(CHPL_COMM_WORLD);
      destroy_plan(arr.fwd);
      destroy_plan(arr.rev);
      destroy_plan(arr.fwdt);
      destroy_plan(arr.revt);
    }
  }
  coforall localeIdx in dom.dist.targetLocDom {
    on locArr(localeIdx) {
      delete locArr(localeIdx);
    }
  }
}

inline proc FFTW3DArr.dsiLocalAccess(i: rank*idxType) ref {
  return myLocArr.this(i);
}

//
// the global accessor for the array
//
// TODO: Do we need a global bounds check here or in targetLocsIdx?
//
// By splitting the non-local case into its own function, we can inline the
// fast/local path and get better performance.
//
// BHARSH TODO: Should this argument have the 'const in' intent? If it is
// remote, the commented-out local block will fail.
//
inline proc FFTW3DArr.dsiAccess(idx: rank*idxType) ref {
  var i = idx;
  local {
    if myLocArr != nil && myLocArr.locDom.member(i) then
      return myLocArr.this(i);
  }
  return nonLocalAccess(i);
}

proc FFTW3DArr.nonLocalAccess(i: rank*idxType) ref {
  if doRADOpt {
    if myLocArr {
      if boundsChecking then
        if !dom.dsiMember(i) then
          halt("array index out of bounds: ", i);
      var rlocIdx = dom.dist.targetLocsIdx(i);
      if !disableFFTW3DLazyRAD {
        if myLocArr.locRAD == nil {
          myLocArr.lockLocRAD();
          if myLocArr.locRAD == nil {
            var tempLocRAD = new LocRADCache(eltType, rank, idxType, stridable, dom.dist.targetLocDom);
            tempLocRAD.RAD.blk = SENTINEL;
            myLocArr.locRAD = tempLocRAD;
          }
          myLocArr.unlockLocRAD();
        }
        if myLocArr.locRAD.RAD(rlocIdx).blk == SENTINEL {
          myLocArr.locRAD.lockRAD(rlocIdx);
          if myLocArr.locRAD.RAD(rlocIdx).blk == SENTINEL {
            myLocArr.locRAD.RAD(rlocIdx) =
              locArr(rlocIdx).myElems._value.dsiGetRAD();
          }
          myLocArr.locRAD.unlockRAD(rlocIdx);
        }
      }
      pragma "no copy" pragma "no auto destroy" var myLocRAD = myLocArr.locRAD;
      pragma "no copy" pragma "no auto destroy" var radata = myLocRAD.RAD;
      if radata(rlocIdx).shiftedDataChunk(0) != nil {
        var dataIdx = radata(rlocIdx).getDataIndex(i);
        return radata(rlocIdx).getDataElem(dataIdx);
      }
    }
  }
  return locArr(dom.dist.targetLocsIdx(i))(i);
}

proc FFTW3DArr.dsiAccess(i: idxType...rank) ref
  return dsiAccess(i);

iter FFTW3DArr.these() ref {
  for i in dom do
    yield dsiAccess(i);
}

//
// TODO: Rewrite this to reuse more of the global domain iterator
// logic?  (e.g., can we forward the forall to the global domain
// somehow?
//
iter FFTW3DArr.these(param tag: iterKind) where tag == iterKind.leader {
  for followThis in dom.these(tag) do
    yield followThis;
}

proc FFTW3DArr.dsiStaticFastFollowCheck(type leadType) param
  return leadType == this.type || leadType == this.dom.type;

proc FFTW3DArr.dsiDynamicFastFollowCheck(lead: [])
  return lead.domain._value == this.dom;

proc FFTW3DArr.dsiDynamicFastFollowCheck(lead: domain)
  return lead._value == this.dom;

iter FFTW3DArr.these(param tag: iterKind, followThis, param fast: bool = false) ref where tag == iterKind.follower {
  proc anyStridable(rangeTuple, param i: int = 1) param
      return if i == rangeTuple.size then rangeTuple(i).stridable
             else rangeTuple(i).stridable || anyStridable(rangeTuple, i+1);

  if chpl__testParFlag {
    if fast then
      chpl__testParWriteln("FFTW3D array fast follower invoked on ", followThis);
    else
      chpl__testParWriteln("FFTW3D array non-fast follower invoked on ", followThis);
  }

  if testFastFollowerOptimization then
    writeln((if fast then "fast" else "regular") + " follower invoked for FFTW3D array");

  var myFollowThis: rank*range(idxType=idxType, stridable=stridable || anyStridable(followThis));
  var lowIdx: rank*idxType;

  for param i in 1..rank {
    var stride = dom.whole.dim(i).stride;
    // NOTE: Not bothering to check to see if these can fit into idxType
    var low = followThis(i).low * abs(stride):idxType;
    var high = followThis(i).high * abs(stride):idxType;
    myFollowThis(i) = ((low..high by stride) + dom.whole.dim(i).alignedLow by followThis(i).stride).safeCast(myFollowThis(i).type);
    lowIdx(i) = myFollowThis(i).low;
  }

  const myFollowThisDom = {(...myFollowThis)};
  if fast {
    //
    // TODO: The following is a buggy hack that will only work when we're
    // distributing across the entire Locales array.  I still think the
    // locArr/locDoms arrays should be associative over locale values.
    //
    var arrSection = locArr(dom.dist.targetLocsIdx(lowIdx));

    //
    // if arrSection is not local and we're using the fast follower,
    // it means that myFollowThisDom is empty; make arrSection local so
    // that we can use the local block below
    //
    if arrSection.locale.id != here.id then
      arrSection = myLocArr;

    //
    // Slicing arrSection.myElems will require reference counts to be updated.
    // If myElems is an array of arrays, the inner array's domain or dist may
    // live on a different locale and require communication for reference
    // counting. Simply put: don't slice inside a local block.
    //
    ref chunk = arrSection.myElems(myFollowThisDom);
    local {
      for i in chunk do yield i;
    }
  } else {
    //
    // we don't necessarily own all the elements we're following
    //
    for i in myFollowThisDom {
      yield dsiAccess(i);
    }
  }
}

proc FFTW3DArr.dsiSerialRead(f) {
  chpl_serialReadWriteRectangular(f, this);
}

//
// output array
//
proc FFTW3DArr.dsiSerialWrite(f) {
  type strType = chpl__signedType(idxType);
  var binary = f.binary();
  if dom.dsiNumIndices == 0 then return;
  var i : rank*idxType;
  for dim in 1..rank do
    i(dim) = dom.dsiDim(dim).low;
  label next while true {
    f.write(dsiAccess(i));
    if i(rank) <= (dom.dsiDim(rank).high - dom.dsiDim(rank).stride:strType) {
      if ! binary then f.write(" ");
      i(rank) += dom.dsiDim(rank).stride:strType;
    } else {
      for dim in 1..rank-1 by -1 {
        if i(dim) <= (dom.dsiDim(dim).high - dom.dsiDim(dim).stride:strType) {
          i(dim) += dom.dsiDim(dim).stride:strType;
          for dim2 in dim+1..rank {
            f.writeln();
            i(dim2) = dom.dsiDim(dim2).low;
          }
          continue next;
        }
      }
      break;
    }
  }
}

pragma "no copy return"
proc FFTW3DArr.dsiLocalSlice(ranges) {
  var low: rank*idxType;
  for param i in 1..rank {
    low(i) = ranges(i).low;
  }
  return locArr(dom.dist.targetLocsIdx(low)).myElems((...ranges));
}

proc _extendTuple(type t, idx: _tuple, args) {
  var tup: args.size*t;
  var j: int = 1;

  for param i in 1..args.size {
    if isCollapsedDimension(args(i)) then
      tup(i) = args(i);
    else {
      tup(i) = idx(j);
      j += 1;
    }
  }
  return tup;
}

proc _extendTuple(type t, idx, args) {
  var tup: args.size*t;
  var idxTup = (idx,);
  var j: int = 1;

  for param i in 1..args.size {
    if isCollapsedDimension(args(i)) then
      tup(i) = args(i);
    else {
      tup(i) = idxTup(j);
      j += 1;
    }
  }
  return tup;
}

proc FFTW3DArr.dsiReallocate(bounds:rank*range(idxType,BoundedRangeType.bounded,stridable)) {
  //
  // For the default rectangular array, this function changes the data
  // vector in the array class so that it is setup once the default
  // rectangular domain is changed.  For this distributed array class,
  // we don't need to do anything, because changing the domain will
  // change the domain in the local array class which will change the
  // data in the local array class.  This will only work if the domain
  // we are reallocating to has the same distribution, but domain
  // assignment is defined so that only the indices are transferred.
  // The distribution remains unchanged.
  //
}

proc FFTW3DArr.dsiPostReallocate() {
  // Call this *after* the domain has been reallocated
  if doRADOpt then setupRADOpt();
}

proc FFTW3DArr.setRADOpt(val=true) {
  doRADOpt = val;
  if doRADOpt then setupRADOpt();
}

proc LocFFTW3DArr.setup() {
  // Maybe we should propagate Ng into locDom?
  const Ng : int = locDom.myBlock.dim(2).size;
  const idx = locDom.myBlock.low;

  Barrier(CHPL_COMM_WORLD);
  fwd = fftw_mpi_plan_dft_r2c_3d(Ng, Ng, Ng,
              myElems[idx], myElems[idx], CHPL_COMM_WORLD, fftwPlanner);
  rev = fftw_mpi_plan_dft_c2r_3d(Ng, Ng, Ng,
              myElems[idx], myElems[idx], CHPL_COMM_WORLD, fftwPlanner);
  fwdt = fftw_mpi_plan_dft_r2c_3d(Ng, Ng, Ng,
              myElems[idx], myElems[idx], CHPL_COMM_WORLD, fftwPlanner|FFTW_MPI_TRANSPOSED_OUT);
  revt = fftw_mpi_plan_dft_c2r_3d(Ng, Ng, Ng,
              myElems[idx], myElems[idx], CHPL_COMM_WORLD, fftwPlanner|FFTW_MPI_TRANSPOSED_IN);
}

//
// the accessor for the local array -- assumes the index is local
//
// TODO: Should this be inlined?
//
inline proc LocFFTW3DArr.this(i) ref {
  return myElems(i);
}

//
// Privatization
//
proc FFTW3D.FFTW3D(other: FFTW3D, privateData,
                param rank = other.rank,
                type idxType = other.idxType,
                type sparseLayoutType = other.sparseLayoutType) {
  boundingBox = {(...privateData(1))};
  targetLocDom = {(...privateData(2))};
  dataParTasksPerLocale = privateData(3);
  dataParIgnoreRunningTasks = privateData(4);
  dataParMinGranularity = privateData(5);
  this.Ng = privateData(6);
  this.slabSize = this.Ng/numLocales;
  this.nghosts = privateData(7);

  for i in targetLocDom {
    targetLocales(i) = other.targetLocales(i);
    locDist(i) = other.locDist(i);
  }
}

proc FFTW3D.dsiSupportsPrivatization() param return true;

proc FFTW3D.dsiGetPrivatizeData() {
  return (boundingBox.dims(), targetLocDom.dims(),
          dataParTasksPerLocale, dataParIgnoreRunningTasks, dataParMinGranularity,
          Ng, nghosts);
}

proc FFTW3D.dsiPrivatize(privatizeData) {
  return new FFTW3D(this, privatizeData);
}

proc FFTW3D.dsiGetReprivatizeData() return boundingBox.dims();

proc FFTW3D.dsiReprivatize(other, reprivatizeData) {
  boundingBox = {(...reprivatizeData)};
  targetLocDom = other.targetLocDom;
  targetLocales = other.targetLocales;
  locDist = other.locDist;
  dataParTasksPerLocale = other.dataParTasksPerLocale;
  dataParIgnoreRunningTasks = other.dataParIgnoreRunningTasks;
  dataParMinGranularity = other.dataParMinGranularity;
}

proc FFTW3DDom.dsiSupportsPrivatization() param return true;

proc FFTW3DDom.dsiGetPrivatizeData() return (dist.pid, whole.dims());

proc FFTW3DDom.dsiPrivatize(privatizeData) {
  var privdist = chpl_getPrivatizedCopy(dist.type, privatizeData(1));
  // in constructor we have to pass sparseLayoutType as it has no default value
  var c = new FFTW3DDom(rank=rank, idxType=idxType, stridable=stridable,
      sparseLayoutType=privdist.sparseLayoutType, dist=privdist);
  for i in c.dist.targetLocDom do
    c.locDoms(i) = locDoms(i);
  c.whole = {(...privatizeData(2))};
  return c;
}

proc FFTW3DDom.dsiGetReprivatizeData() return whole.dims();

proc FFTW3DDom.dsiReprivatize(other, reprivatizeData) {
  for i in dist.targetLocDom do
    locDoms(i) = other.locDoms(i);
  whole = {(...reprivatizeData)};
}

proc FFTW3DArr.dsiSupportsPrivatization() param return true;

proc FFTW3DArr.dsiGetPrivatizeData() return dom.pid;

proc FFTW3DArr.dsiPrivatize(privatizeData) {
  var privdom = chpl_getPrivatizedCopy(dom.type, privatizeData);
  var c = new FFTW3DArr(eltType=eltType, rank=rank, idxType=idxType,
      stridable=stridable, sparseLayoutType=sparseLayoutType, dom=privdom);
  for localeIdx in c.dom.dist.targetLocDom {
    c.locArr(localeIdx) = locArr(localeIdx);
    if c.locArr(localeIdx).locale.id == here.id then
      c.myLocArr = c.locArr(localeIdx);
  }
  return c;
}

proc FFTW3DArr.dsiSupportsBulkTransfer() param {
  if sparseLayoutType != DefaultDist then
    return false;
  else
    return true;
}
proc FFTW3DArr.dsiSupportsBulkTransferInterface() param {
   if sparseLayoutType != DefaultDist then
    return false;
  else
    return true;
}

proc FFTW3DArr.doiCanBulkTransfer(viewDom) {
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiCanBulkTransfer");

  if viewDom.stridable then
    for param i in 1..rank do
      if viewDom.dim(i).stride != 1 then return false;

  // See above note regarding aliased arrays
  if disableAliasedBulkTransfer then
    if _arrAlias != nil then return false;

  return true;
}

proc FFTW3DArr.doiCanBulkTransferStride(viewDom) param {
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiCanBulkTransferStride");

  return useBulkTransferDist;
}

proc FFTW3DArr.oneDData {
  if defRectSimpleDData {
    return true;
  } else {
    // TODO: do this when we create the array?  if not, then consider:
    // TODO: use locRad oneDData, if available
    // TODO: with more coding complexity we could get a much quicker
    //       answer in the 'false' case, but how to avoid penalizing
    //       the 'true' case at scale?
    var allBlocksOneDData: bool;
    on this {
      var myAllBlocksOneDData: atomic bool;
      myAllBlocksOneDData.write(true);
      forall la in locArr {
        if !la.myElems._value.oneDData then
          myAllBlocksOneDData.write(false);
      }
      allBlocksOneDData = myAllBlocksOneDData.read();
    }
    return allBlocksOneDData;
  }
}

proc FFTW3DArr.doiUseBulkTransfer(B) {
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiUseBulkTransfer()");

  //
  // Absent multi-ddata, for the array as a whole, say bulk transfer
  // is possible.  We'll make a final determination for each block in
  // doiBulkTransfer(), based on the characteristics of the blocks
  // themselves.
  //
  // If multi-ddata is possible then we can only do bulk transfer when
  // either the domains are identical (so we can defer the decision as
  // above) or all the blocks of both arrays have but a single ddata
  // chunk.
  //
  if this.rank != B.rank then return false;
  return defRectSimpleDData
         || dom == B._value.dom
         || (oneDData && chpl__getActualArray(B).oneDData);
}

proc FFTW3DArr.doiUseBulkTransferStride(B) {
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiUseBulkTransferStride()");

  //
  // Absent multi-ddata, for the array as a whole, say bulk transfer
  // is possible even though as things are currently coded we'll always
  // do regular bulk transfer and never even ask about strided.
  //
  // If multi-ddata is possible then we can only do strided bulk
  // transfer when all the blocks have but a single ddata chunk.
  //
  if this.rank != B.rank then return false;
  return defRectSimpleDData
         || (oneDData && chpl__getActualArray(B).oneDData);
}

proc FFTW3DArr.doiBulkTransfer(B, viewDom) {
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiBulkTransfer");
  const actual = chpl__getActualArray(B);
  // TODO: how to avoid a deep copy of the domain here? If it's distributed,
  // it can cause a lot of comms...
  pragma "no copy" const actDom = chpl__getViewDom(B);

  if debugFFTW3DDistBulkTransfer then resetCommDiagnostics();

  const equalDoms = (this.dom.whole == actual.dom.whole) &&
                    (actDom == viewDom) &&
                    (this.dom.dist.dsiEqualDMaps(actual.dom.dist));

  // Use zippered iteration to piggyback data movement with the remote
  //  fork.  This avoids remote gets for each access to locArr[i] and
  //  actual.locArr[i]
  coforall (i, myLocArr, BmyLocArr, myLocDom, BLocDom) in zip(dom.dist.targetLocDom,
                                        locArr,
                                        actual.locArr,
                                        this.dom.locDoms,
                                        actual.dom.locDoms) {
    on dom.dist.targetLocales(i) {
      if this.rank == B.rank {
        if debugFFTW3DDistBulkTransfer then startCommDiagnosticsHere();

        const viewBlock = myLocDom.myBlock[viewDom];

        if equalDoms {
          const theirView = BLocDom.myBlock[actDom];
          myLocArr.myElems[viewBlock] = BmyLocArr.myElems[theirView];
        } else if this.rank == 1 {
          var start = viewBlock.low;

          for (rid, rlo, size) in ConsecutiveChunks(viewDom, actual.dom, actDom, viewBlock.size, start) {
            myLocArr.myElems[start..#size] = actual.locArr[rid].myElems[rlo..#size];
            start += size;
          }
        } else {
          const orig = viewBlock.low(rank);

          for coord in dropDims(viewBlock, viewBlock.rank) {
            var lo = if rank == 2 then (coord, orig) else ((...coord), orig);

            for (rid, rlo, size) in ConsecutiveChunksD(viewDom, actual.dom, actDom, viewBlock.dim(rank).length, lo) {
              var LSlice, RSlice : rank*range(idxType = this.dom.idxType);

              for param i in 1..rank-1 {
                LSlice(i) = if rank == 2 then coord..coord else coord(i)..coord(i);
                RSlice(i) = rlo(i)..rlo(i);
              }
              LSlice(rank) = lo(rank)..#size;
              RSlice(rank) = rlo(rank)..#size;

              myLocArr.myElems[(...LSlice)] = actual.locArr[rid].myElems[(...RSlice)];

              lo(rank) += size;
            }
          }
        }

        if debugFFTW3DDistBulkTransfer then stopCommDiagnosticsHere();
      } else {
        halt("Bulk-transfer called with FFTW3D of differing rank!");
      }
    }
  }
  if debugFFTW3DDistBulkTransfer then writeln("Comms:",getCommDiagnostics());
}

proc FFTW3DArr.dsiTargetLocales() {
  return dom.dist.targetLocales;
}

proc FFTW3DDom.dsiTargetLocales() {
  return dist.targetLocales;
}

proc FFTW3D.dsiTargetLocales() {
  return targetLocales;
}

// FFTW3D subdomains are continuous

proc FFTW3DArr.dsiHasSingleLocalSubdomain() param return true;
proc FFTW3DDom.dsiHasSingleLocalSubdomain() param return true;

// returns the current locale's subdomain

proc FFTW3DArr.dsiLocalSubdomain() {
  return myLocArr.locDom.myBlock;
}
proc FFTW3DDom.dsiLocalSubdomain() {
  // TODO -- could be replaced by a privatized myLocDom in FFTW3DDom
  // as it is with FFTW3DArr
  var myLocDom:LocFFTW3DDom(rank, idxType, stridable) = nil;
  for (loc, locDom) in zip(dist.targetLocales, locDoms) {
    if loc == here then
      myLocDom = locDom;
  }
  return myLocDom.myBlock;
}

iter ConsecutiveChunks(LView, RDomClass, RView, len, in start) {
  var elemsToGet = len;
  const offset   = RView.low - LView.low;
  var rlo        = start + offset;
  var rid        = RDomClass.dist.targetLocsIdx(rlo);
  while elemsToGet > 0 {
    const size = min(RDomClass.numRemoteElems(RView, rlo, rid), elemsToGet);
    yield (rid, rlo, size);
    rid += 1;
    rlo += size;
    elemsToGet -= size;
  }
}

iter ConsecutiveChunksD(LView, RDomClass, RView, len, in start) {
  param rank     = LView.rank;
  var elemsToGet = len;
  const offset   = RView.low - LView.low;
  var rlo        = start + offset;
  var rid        = RDomClass.dist.targetLocsIdx(rlo);
  while elemsToGet > 0 {
    const size = min(RDomClass.numRemoteElems(RView, rlo(rank):int, rid(rank):int), elemsToGet);
    yield (rid, rlo, size);
    rid(rank) +=1;
    rlo(rank) += size;
    elemsToGet -= size;
  }
}

proc FFTW3DDom.numRemoteElems(viewDom, rlo, rid) {
  // NOTE: Not bothering to check to see if rid+1, length, or rlo-1 used
  //  below can fit into idxType
  var blo, bhi:dist.idxType;
  if rid==(dist.targetLocDom.dim(rank).length - 1) then
    bhi=viewDom.dim(rank).high;
  else {
      bhi = dist.boundingBox.dim(rank).low +
        intCeilXDivByY((dist.boundingBox.dim(rank).high - dist.boundingBox.dim(rank).low +1)*(rid+1):idxType,
                       dist.targetLocDom.dim(rank).length:idxType) - 1:idxType;
  }

  return (bhi - (rlo - 1):idxType);
}

//Brad's utility function. It drops from Domain D the dimensions
//indicated by the subsequent parameters dims.
proc dropDims(D: domain, dims...) {
  var r = D.dims();
  var r2: (D.rank-dims.size)*r(1).type;
  var j = 1;
  for i in 1..D.rank do
    for k in 1..dims.size do
      if dims(k) != i {
        r2(j) = r(i);
        j+=1;
      }
  var DResult = {(...r2)};
  return DResult;
}

//For assignments of the form: "FFTW3D = any"
//where "any" means any array that implements the bulk transfer interface
// TODO: avoid spawning so many coforall-ons
//   - clean up some of this range creation logic
proc FFTW3DArr.doiBulkTransferFrom(Barg, viewDom)
{
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiBulkTransferFrom()");

  if this.rank == Barg.rank {
    const Dest = this;
    const Src = chpl__getActualArray(Barg);
    const srcView = chpl__getViewDom(Barg);
    type el = Dest.idxType;
    coforall i in Dest.dom.dist.targetLocDom {
      on Dest.dom.dist.targetLocales(i) {
        var regionDest = Dest.dom.locDoms(i).myBlock[viewDom];
        var regionSrc = Src.dom.locDoms(i).myBlock[srcView];
        if regionDest.numIndices>0
        {
          const ini=bulkCommConvertCoordinate(regionDest.first, viewDom, srcView);
          const end=bulkCommConvertCoordinate(regionDest.last, viewDom, srcView);
          const sb=chpl__tuplify(regionSrc.stride);

          var r1,r2: rank * range(idxType = el,stridable = true);
          r2=regionDest.dims();
           //In the case that the number of elements in dimension t for r1 and r2
           //were different, we need to calculate the correct stride in r1
          for param t in 1..rank{
              r1[t] = (ini[t]:el..end[t]:el by sb[t]:el);
              if r1[t].length != r2[t].length then
                r1[t] = (ini[t]:el..end[t]:el by (end[t] - ini[t]):el/(r2[t].length-1));
          }
        
          if debugFFTW3DDistBulkTransfer then
              writeln("Src{",(...r1),"}.ToDR",regionDest);

          Barg._value.doiBulkTransferToDR(Dest.locArr[i].myElems[regionDest], {(...r1)});
        }
      }
    }
  }
}
 
//For assignments of the form: DR = FFTW3D 
//(default rectangular array = block distributed array)
proc FFTW3DArr.doiBulkTransferToDR(Barg, viewDom)
{
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiBulkTransferToDR()");

  if this.rank == Barg.rank {
    const Src = this;
    const Dest = chpl__getActualArray(Barg);
    const destView = chpl__getViewDom(Barg);
    type el = Src.idxType;
    coforall j in Src.dom.dist.targetLocDom do
      on Src.dom.dist.targetLocales(j)
      {
        const inters=Src.dom.locDoms(j).myBlock[viewDom];
        if(inters.numIndices>0)
        {
          const ini=bulkCommConvertCoordinate(inters.first, viewDom, destView);
          const end=bulkCommConvertCoordinate(inters.last, viewDom, destView);
          const sa = chpl__tuplify(destView.stride);

          var r1,r2: rank * range(idxType = el,stridable = true);
          for param t in 1..rank
          {
            r2[t] = (chpl__tuplify(inters.first)[t]
                     ..chpl__tuplify(inters.last)[t]
                     by chpl__tuplify(inters.stride)[t]);
            r1[t] = (ini[t]:el..end[t]:el by sa[t]:el);
          }
          
          if debugFFTW3DDistBulkTransfer then
            writeln("A[",r1,"] = B[",r2,"]");
        
          Barg[(...r1)] = Src.locArr[j].myElems[(...r2)];
        }
      }
  }
}

//For assignments of the form: FFTW3D = DR 
//(block distributed array = default rectangular)
proc FFTW3DArr.doiBulkTransferFromDR(Barg, viewDom)
{
  if debugFFTW3DDistBulkTransfer then
    writeln("In FFTW3DArr.doiBulkTransferFromDR");

  if this.rank == Barg.rank {
    const Dest = this;
    const srcView = chpl__getViewDom(Barg);
    type el = Dest.idxType;
    coforall j in Dest.dom.dist.targetLocDom do
      on Dest.dom.dist.targetLocales(j)
      {
        const inters=Dest.dom.locDoms(j).myBlock[viewDom];
        if(inters.numIndices>0)
        {
          const ini=bulkCommConvertCoordinate(inters.first, viewDom, srcView);
          const end=bulkCommConvertCoordinate(inters.last, viewDom, srcView);
          const sb = chpl__tuplify(srcView.stride);
          
          var r1,r2: rank * range(idxType = el,stridable = true);
          for param t in 1..rank
          {
            r2[t] = (chpl__tuplify(inters.first)[t]
                     ..chpl__tuplify(inters.last)[t]
                     by chpl__tuplify(inters.stride)[t]);
            r1[t] = (ini[t]:el..end[t]:el by sb[t]:el);
          }

          if debugFFTW3DDistBulkTransfer then
            writeln("A[",r2,"] = B[",r1,"]");

          Dest.locArr[j].myElems[(...r2)] = Barg[(...r1)];
        }
      }
  }
}

//---------------------------------------------------------------
// NP -- new routines go below
//---------------------------------------------------------------

proc _domain.getNgrid() {
  return _value.dsigetNgrid();
}

proc FFTW3DDom.dsigetNgrid() {
  return dist.Ng;
}

proc _array.localArray() ref {
  return _value.dsilocalArray();
}

proc FFTW3DArr.dsilocalArray() ref {
  return myLocArr.myElems;
}

// FFTW specific routines go below here
proc _array.fftForward(transposeOpt=false) {
  _value.dsifftForward(transposeOpt);
}

proc FFTW3DArr.dsifftForward(transposeOpt:bool) {
  coforall locid in dom.dist.targetLocDom {
    on dom.dist.targetLocales(locid) {
      Barrier(CHPL_COMM_WORLD);
      if transposeOpt then
        execute(locArr[locid].fwdt);
      else 
        execute(locArr[locid].fwd);
    }
  }
}

proc _array.fftReverse(transposeOpt=false) {
  _value.dsifftReverse(transposeOpt);
}

proc FFTW3DArr.dsifftReverse(transposeOpt) {
  coforall locid in dom.dist.targetLocDom {
    on dom.dist.targetLocales(locid) {
      Barrier(CHPL_COMM_WORLD);
      if transposeOpt then
        execute(locArr[locid].revt);
      else 
        execute(locArr[locid].rev);
    }
  }
}

proc addGhostsToDomain(dom, nghosts: int) {
  var newdomain = dom.dims();
  var ilo = newdomain(1).low-nghosts;
  var ihi = newdomain(1).high+nghosts;
  newdomain(1) = ilo..ihi;
  var ret : domain(3, stridable=dom.stridable) = {(...newdomain)};
  return ret;
}

proc _array.updateGhosts() {
  _value.dsiupdateGhosts();
}

proc FFTW3DArr.dsiupdateGhosts() {
  coforall locid in dom.dist.targetLocDom do
    on dom.dist.targetLocales(locid) {
      // get left, right and me
      ref A = locArr[locid].myElems;
      const leftid = (here.id-1+numLocales)%numLocales;
      const rightid = (here.id+1)%numLocales;
      // WARNING! Assuming 3D
      ref lA = locArr[leftid,0,0].myElems;
      ref rA = locArr[rightid,0,0].myElems;

      // Get basic information
      const ilo = here.id*dom.dist.slabSize,
        ihi = ilo + dom.dist.slabSize,
        di = dom.dist.nghosts,
        ng = dom.dist.Ng;

      // Neighbours
      var left = (ilo-di+ng)%ng;
      var right = ihi%ng;

      //var tt : Timer;
      //tt.clear(); tt.start();
      cobegin {
        A[(ilo-di)..#di,..,..] = lA[left..#di,..,..];
        A[ihi..#di,..,..] = rA[right..#di,..,..];
      }
      //tt.stop();
      //writeln("Updateghosts :", tt.elapsed());
  }
}

proc _array.accumFromGhosts() {
  _value.dsiaccumFromGhosts();
}

proc FFTW3DArr.dsiaccumFromGhosts() {
  coforall locid in dom.dist.targetLocDom do
    on dom.dist.targetLocales(locid) {
      // get left, right and me
      ref A = locArr[locid].myElems;
      const leftid = (here.id-1+numLocales)%numLocales;
      const rightid = (here.id+1)%numLocales;
      // WARNING! Assuming 3D
      ref lA = locArr[leftid,0,0].myElems;
      ref rA = locArr[rightid,0,0].myElems;

      // Get basic information
      const ilo = here.id*dom.dist.slabSize,
        ihi = ilo + dom.dist.slabSize,
        di = dom.dist.nghosts,
        ng = dom.dist.Ng;

      // Neighbours
      var left = if (ilo==0) then ng else ilo;
      var right = if (ihi==ng) then 0 else ihi;

      //var tt : Timer;
      //tt.clear(); tt.start();
      /* Direct accumulating is very slow. Try pre-fetching. */
      cobegin {
        {
          var tmp1 = lA[left..#di,..,..];
          A[ilo..#di,..,..] += tmp1;
          //A[ilo..#di,..,..] += lA[left..#di,..,..];
        }
        {
          var tmp2 = rA[(right-di)..#di,..,..];
          A[(ihi-di)..#di,..,..] += tmp2;
          //A[(ihi-di)..#di,..,..] += rA[(right-di)..#di,..,..];
        }
      }
      //tt.stop();
      //writeln("Accumghosts :", tt.elapsed());
  }
}

proc _array.zeroGhosts() {
  _value.dsizeroGhosts();
}

proc FFTW3DArr.dsizeroGhosts() {
  coforall locid in dom.dist.targetLocDom do
    on dom.dist.targetLocales(locid) {
      // get left, right and me
      ref A = locArr[locid].myElems;

      // Get basic information
      const ilo = here.id*dom.dist.slabSize,
        ihi = ilo + dom.dist.slabSize,
        di = dom.dist.nghosts;

      cobegin {
        A[(ilo-di)..#di,..,..] = 0.0;
        A[ihi..#di,..,..] = 0.0;
      }
  }
}


proc makeFFTWDomains(Ng: int, nghosts: int=0) {
  if (Ng%2!=0) then
    halt("Ng must be divisible by 2");
  if (Ng%numLocales!=0) then
    halt("Ng must be divisible by numLocales");

  // Define the domains
  const DSpace = {0.. #Ng, 0.. #Ng, 0.. #(Ng+2)};
  const D : domain(3) dmapped FFTW3D(Ng, nghosts=nghosts) = DSpace;
  const Dreal = D[..,..,0.. #Ng]; // In real space
  const Dre = D[..,..,0.. #(Ng+2) by 2 align 0]; // Real part
  const Dim = D[..,..,0.. #(Ng+2) by 2 align 1]; // Imaginary part
  const Dk = D[..,..,0.. #(Ng/2+1)]; // Frequency

  return (D, Dreal, Dre, Dim, Dk);
}

inline proc kFreq(ik : 3*int, Ng : int) : 3*int {
  var kk = ik;
  for param idim in 1..3 {
    ref k1 = kk(idim);
    if (k1 > Ng/2) then k1 = k1-Ng;
  }
  return kk;
}

proc isFFTWDist(A: [?d]) param {
  proc isFFTWClass(dc: FFTW3D) param return true;
  proc isFFTWClass(dc) param return false;
  return isFFTWClass(d._value.dist);
}
