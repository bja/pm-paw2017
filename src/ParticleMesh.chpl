module ParticleMesh {

  use FFTWDist;
  use ParticleUtils;
  use ndfilehandler;
  //use PrivateDist;


  proc CIC(const ref PP: Particles, ref GG: [?D]real) {
    // Zero the ghost cells. The CIC operation makes little sense without this.
    GG.zeroGhosts();

    // Define an auxiliary array, with atomic semantics
    // Chapel initializes arrays, so we don't need to worry here.
    var rho1: [D] atomic real; 
    // Zero explicitly -- since these are atomics
    coforall loc in Locales {
      on loc {
        ref arr = rho1.localArray();
        forall x in arr do x.write(0.0);
      }
    }

    // Get the grid size 
    const Ng = D.getNgrid();
    const Ng1 = 0.99999999*Ng; // Protect against roundoff error

    forall rr in PP.Coords() { 
      local {
        const (x1, y1, z1) = (rr(1)*Ng1, rr(2)*Ng1, rr(3)*Ng1);
        const
          ix = x1 : int,
          iy = y1 : int,
          iz = z1 : int;
        const
          iix = ix+1, // Use the ghost cell
          iiy = (iy+1)%Ng,
          iiz = (iz+1)%Ng;
        const
          dx = x1-ix,
          dy = y1-iy,
          dz = z1-iz,
          idx = 1.0-dx,
          idy = 1.0-dy,
          idz = 1.0-dz;
        rho1.localAccess(ix ,  iy,  iz).add(idx * idy * idz);
        rho1.localAccess(ix ,  iy, iiz).add(idx * idy *  dz);
        rho1.localAccess(ix , iiy,  iz).add(idx *  dy * idz);
        rho1.localAccess(ix , iiy, iiz).add(idx *  dy *  dz);
        rho1.localAccess(iix,  iy,  iz).add( dx * idy * idz); 
        rho1.localAccess(iix,  iy, iiz).add( dx * idy *  dz);
        rho1.localAccess(iix, iiy,  iz).add( dx *  dy * idz);
        rho1.localAccess(iix, iiy, iiz).add( dx *  dy *  dz);
      }
    }

    // Now transfer the atomic data back to full array
    // We need to be a little careful here to copy the ghost values as well.
    coforall loc in Locales do
      on loc {
        ref src = rho1.localArray();
        ref dest = GG.localArray();
        [(x1, y1) in zip(src,dest)] y1 += x1.read(); // x1 is atomic
      }


    // Now call accumGhosts
    GG.accumFromGhosts();
  }


  /* arr stores where we want to interpolate to */
  proc interpolateFromGrid(ref PP: Particles, const ref rho: [?D]real, ref arr : SkylineArr(?T)) {
    const Ng = D.getNgrid();

    forall rr in PP.Coords(arr) {
      local {
        const (x1, y1, z1) = (rr(1)*Ng, rr(2)*Ng, rr(3)*Ng);
        const
          ix = x1 : int,
          iy = y1 : int,
          iz = z1 : int;
        const
          iix = ix+1, // Use the ghost cell
          iiy = (iy+1)%Ng,
          iiz = (iz+1)%Ng;
        const
          dx = x1-ix,
          dy = y1-iy,
          dz = z1-iz,
          idx = 1.0-dx,
          idy = 1.0-dy,
          idz = 1.0-dz;
        var val = 0.0;
        val+=rho.localAccess(ix ,  iy,  iz)*idx * idy * idz;
        val+=rho.localAccess(ix ,  iy, iiz)*idx * idy *  dz;
        val+=rho.localAccess(ix , iiy,  iz)*idx *  dy * idz;
        val+=rho.localAccess(ix , iiy, iiz)*idx *  dy *  dz;
        val+=rho.localAccess(iix,  iy,  iz)* dx * idy * idz; 
        val+=rho.localAccess(iix,  iy, iiz)* dx * idy *  dz;
        val+=rho.localAccess(iix, iiy,  iz)* dx *  dy * idz;
        val+=rho.localAccess(iix, iiy, iiz)* dx *  dy *  dz;

        rr(4) = val:T;
      }
    }
  }


  /*---------------------------------------------------
    Power Spectrum Routines.
    ---------------------------------------------------*/
  record PkRecord {
    var kmin, kmax : real;
    var nbins : int;
    var dk, invdk : real; 

    var D : domain(1);
    var kval, delta2, nmodes: [D]real;

    proc PkRecord(kmin:real, kmax:real, nbins:int) {
      this.kmin = kmin;
      this.kmax = kmax;
      this.nbins = nbins;

      dk = (kmax-kmin)/nbins;
      invdk = 1.0/dk;
      D = {0.. #nbins};
    }

    /*
      kfac allows a rescaling of k, for convenience
    */
    proc writeFileHandler(fn:string, kfac:real=1.0) {
      var klo, khi : [D]real;
      [ik in D] { klo[ik] = ik*dk*kfac+kmin; khi[ik] = klo[ik]+dk; }
      var k0 = kval*kfac;
      writeColumn(klo, fn, "klo");
      writeColumn(khi, fn, "khi");
      writeColumn(k0, fn, "kval");
      writeColumn(delta2, fn, "delta2");
      writeColumn(nmodes, fn, "nmodes");
    }

  }


  // Spherical bessel function
  proc j0(x: real) {
    if (x > 0.05) {
      return sin(x)/x;
    } else {
      const x2 = x*x;
      return 1+x2*(-1.0/6.0 + x2*(1.0/120.0 - x2/5040.0));
    }
  }


  proc computePk(PP: Particles, GG: []real,
                 kmin:real, kmax:real, nbins:int): PkRecord
    where isFFTWDist(GG) {
    // Count the number of particles
    const nptot = PP.size();

    // Put the particles onto the grid
    GG = 0; // Zero the grid -- CIC adds to the grid
    CIC(PP, GG); // CIC handles all the zeroing of ghosts etc
    const Ng = GG.domain.getNgrid();
    const fac = (Ng:real**3)/(nptot:real);
    GG = GG*fac - 1.0;

    return computePk(GG, kmin, kmax, nbins);
  }

  /*
   */
  // TODO -- should enforce GG having right type
  proc computePk(GG: [?D]real,
                 kmin:real, kmax:real, nbins:int): PkRecord
    where isFFTWDist(GG) {
    // FFT
    GG.fftForward();

    // Now compute the power spectrum
    var pk = new PkRecord(kmin, kmax, nbins);
    ref kval = pk.kval;
    ref delta2 = pk.delta2;
    ref nmodes = pk.nmodes;
    const invdk = pk.invdk;
    kval = 0.0;
    delta2 = 0.0;
    nmodes = 0.0;

    const Ng = D.getNgrid();
    const Dre = D[..,..,0..#(Ng+2) by 2 align 0];
    const Dim = D[..,..,0..#(Ng+2) by 2 align 1];
    const Dk = D[..,..,0.. #(Ng/2+1)];

    forall (ik, ire, iim) in zip(Dk, Dre, Dim)
        with (+ reduce kval,
              + reduce delta2,
              + reduce nmodes) {
      // local { // TODO: This should be possible, see issue #7
        const scale = 1.0/(Ng:real**6);
        const twopi2 = (2.0*pi)**2;
        var kk = kFreq(ik, Ng);
        var k2 = 0.0;
        for param idim in 1..Ndim do k2 += kk(idim)**2;
        k2 *= twopi2;
        const k1 = sqrt(k2);
        const ikbin = ((k1 - kmin)*invdk):int;
        if (ikbin >= 0) && (ikbin < nbins) {
          const fac = if ((ik(3)==0)||(ik(3)==Ng/2)) then 1.0 else 2.0;
          kval[ikbin] += k1*fac;
          const d2 = (GG[ire]**2 + GG[iim]**2)*scale*k2*k1/(0.5*twopi2);
          var window = j0(pi*kk(1)/Ng)**2;
          window *= j0(pi*kk(2)/Ng)**2;
          window *= j0(pi*kk(3)/Ng)**2;
          delta2[ikbin] += d2*fac/window**2;
          nmodes[ikbin] += fac;
        }
        //} End local block
    }

    var nmodes1 = nmodes+1.0e-20;
    kval /= nmodes1;
    delta2 /= nmodes1;

    return pk;
  }




  // End module ParticleMesh
}