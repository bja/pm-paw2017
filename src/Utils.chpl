module Utils {

  use ReplicatedDist;
  use SysCTypes;
  require "gsl/gsl_spline.h";

  
  /* Assumes uniformly sampled points

     We use a record here, to help Chapel copy it around.
  */
  record LinearInterpolation {
    var D: domain(1) dmapped Replicated();
    var ytab : [D] real;
    var xmin, xmax, factor: real;


    // Old-style constructor
    proc LinearInterpolation(xx: [?D1]real, yy: [D1]real) {
      D = 0..#D1.size;
      // Replicate across all locales
      coforall loc in Locales do on loc do ytab = yy;
      xmin = xx[D1.low];
      xmax = xx[D1.high];
      factor = (D.size-1.0)/(xmax-xmin);
    }

    // No checks
    inline proc this(x: real): real {
      var dindx = factor*(x-xmin);
      var k = dindx:int;
      dindx -= k;
      return ytab[k] + dindx*(ytab[k+1]-ytab[k]);
    }

  }


  // GSL routines we want
  extern type gsl_spline;
  extern type gsl_interp_accel;
  extern type gsl_interp_type;
  extern const gsl_interp_cspline : c_ptr(gsl_interp_type);

  extern proc gsl_interp_accel_alloc() : c_ptr(gsl_interp_accel);
  extern proc gsl_interp_accel_free(acc : c_ptr(gsl_interp_accel));
  extern proc gsl_spline_alloc(tt: c_ptr(gsl_interp_type), size : size_t) : c_ptr(gsl_spline);
  extern proc gsl_spline_free(spline: c_ptr(gsl_spline));
  extern proc gsl_spline_init(spline: c_ptr(gsl_spline), xa :[]real, ya :[]real, size: size_t);
  extern proc gsl_spline_eval(spline: c_ptr(gsl_spline), x :real, acc: c_ptr(gsl_interp_accel)): real;


}