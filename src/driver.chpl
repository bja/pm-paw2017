/* This is the main driver program. */

use MPI;

use Cosmo;
use FFTWDist;
use ParticleUtils;
use ParticleMesh;

use Time;
use Random;

// Define a global timer
var timeit : Timer;


config const iseed=1234;
config const StepSize1 = 0.03;

// Power spectrum configuration constants
config const pk_kmin = 2.0;
config const pk_kmax = 100.0;
config const pk_nbins = 25;
config const pk_prefix="output";


/* Global storage for particles and grids.
    Yes, I know globals are bad, but hey, that's how
    science is done!

   We start the particles on a grid.

   We'll need two separate grids. One of these will need
   2 ghost cells, while the other will just need 1.
*/
timeit.clear(); timeit.start();
const DomainTuple1 = makeFFTWDomains(Ng, nghosts=1);
const DomainTuple2 = makeFFTWDomains(Ng, nghosts=2);
var AA: [DomainTuple1(1)]real;
var BB: [DomainTuple2(1)]real;


// Perturb the particles slightly off the grid.
const displace = (0.01:real(32),
                  0.01:real(32),
                  0.01:real(32));
var PP = initializeParticlesOnGrid(Nc, displace=displace);
timeit.stop();
const initTime=timeit.elapsed();




proc main() {
  // Print useful information at the beginning
  writef("PM_Chapel, running on %i locales..\n",numLocales);
  writef("OmegaM=%r, OmegaX=%r, weff=%r\n",omegam, omegax, weff);
  writef("Boxsize=%r\n",Lbox);
  writef("Ngrid=%i, Nparticles=%i\n",Ng,Npart);
  writef("Initial a=%r (z=%r)\n",ainit, zinit);
  writef("Seed=%i\n",iseed);
  writeln();
  writef("Data structure initialization time : %r (s)\n",initTime);

  stdout.flush();

  // Initial conditions
  makeZeldovichInitialConditions();
  slabDecompose(PP);
  {
    var pk = computePk(PP, AA, kmin=pk_kmin*pi, kmax=pk_kmax*pi, nbins=pk_nbins);
    pk.writeFileHandler("%s_ic".format(pk_prefix));
  }
  stdout.flush();

  // More timers
  var slabTime, forceTime, kickTime, streamTime : Timer;
  slabTime.clear();
  forceTime.clear();
  kickTime.clear();
  streamTime.clear();
  

  // The world's simplest evolution
  timeit.clear(); timeit.start();
  var aa = ainit;
  var dloga = StepSize1;
  var iterstep = 0;
  const outputfn = "%s_%6.4dr".format(pk_prefix, afinal);
  do {
    // Diagnostic information
    //PP.printParticleStats();

    var ahalf = aa*exp(0.5*dloga); 

    // Stream particles 
    // Two cases --- do I cross the final a or not.
    var donePk : bool = false;
    if (ahalf < afinal) {
      streamTime.start();
      streamParticles(log(aa), log(ahalf));
      streamTime.stop();
    } else {
      // Do this in two steps 
      streamTime.start();
      streamParticles(log(aa), log(afinal));
      streamTime.stop();
      slabDecompose(PP);
      var pk = computePk(PP, AA, kmin=pk_kmin*pi, kmax=pk_kmax*pi, nbins=pk_nbins);
      pk.writeFileHandler(outputfn);
      streamTime.start();
      streamParticles(log(afinal), log(ahalf));
      streamTime.stop();
      donePk = true;
    }


    // Kick particles
    slabTime.start();
    slabDecompose(PP);
    slabTime.stop();
    forceTime.start();
    pmforce(aa);
    forceTime.stop();
    kickTime.start();
    kickParticles(log(aa), log(aa)+dloga);
    kickTime.stop();

    // Stream particles
    if ((log(aa)+dloga) < afinal) || donePk {
      streamTime.start();
      streamParticles(log(ahalf), log(aa)+dloga);
      streamTime.stop();
    } else {
      streamTime.start();
      streamParticles(log(ahalf), log(afinal));
      streamTime.stop();
      slabDecompose(PP);
      var pk = computePk(PP, AA, kmin=pk_kmin, kmax=pk_kmax, nbins=pk_nbins);
      pk.writeFileHandler(outputfn);
      streamTime.start();
      streamParticles(log(afinal), log(aa)+dloga);
      streamTime.stop();
      donePk = true;
    }

    // Update
    aa *= exp(dloga);
    iterstep+= 1;

    // Print some information
    writef("%i : aa=%r   time=%r (s)", iterstep, aa, timeit.elapsed());
    writef(" %r %r %r %r \n",
           streamTime.elapsed(),
           kickTime.elapsed(),
           slabTime.elapsed(),
           forceTime.elapsed());
    stdout.flush();
  } while (aa <= afinal);

  writeln();
  writeln("PM Code ends here");


  // May as well clean up here...
  delete PP;
}




proc makeGaussianPotential(GG: [?D]real) where isFFTWDist(AA) {
  // Define some useful domains
  const Dre = D[..,..,.. by 2 align 0];
  const Dim = D[..,..,.. by 2 align 1];
  const Dk =  D[..,..,0..#(Ng/2+1)]; 

  fillRandom(GG, seed=iseed);
  const twopi = 2*pi;
  const norm = 1.0/Ng:real**1.5; // This normalization yields unit Gaussian complex #s

  // Fill the grid with Gaussian random variables
  forall (x0, x1) in zip(GG[Dre], GG[Dim]) {
    // Box-Muller
    x0 += if (x0 < 1.0e-30) then 1.0e-30 else 0.0;
    const rr = sqrt(-2*log(x0));
    const ang = twopi*x1;
    x0 = rr*cos(ang)*norm;
    x1 = rr*sin(ang)*norm;
  }
  GG.fftForward();

  /* Now build the potential field.

     We will zero all power beyond the Nyquist frequency.

     Note that the LinearInterpolation class is multi-locale aware,
     so this doesn't incur any additional communication.
  */
  const pkInterp = loadInitialPowerSpectrum();
  writeln("Loaded initial power spectrum...");
  const kNyq = Ng*pi;
  const twopi2 = twopi**2;
  forall (ik, gre, gim) in zip(Dk, GG[Dre], GG[Dim]){
    local { // For safety
      const kk = kFreq(ik, Ng);
      var k2 = 0.0;
      for param idim in 1..Ndim do k2 += kk[idim]**2;
      k2 *= twopi2;
      const k1 = sqrt(k2);
      if ((k1 > kNyq) || (k1 < 0.01))  {
        gre = 0.0; gim = 0.0;
      } else {
        const fac = pkInterp(k1)/k2;
        gre *= fac;
        gim *= fac;
      }
    } // End of local
  }


  // End of makeGaussianPotential
}

proc makeZeldovichInitialConditions() {
  // The particles are automatically on the grid

  // Generate an initial Gaussian density field
  timeit.clear(); timeit.start();
  /* Use BB; we can then use AA which has only 1 ghost when
     we compute the velocities.
  */
  makeGaussianPotential(BB);
  timeit.stop();
  writef("Time to generate the IC potential field: %r (s)\n",timeit.elapsed());

  /* Now we compute the displacements */
  const Dre = DomainTuple1(3),
    Dim = DomainTuple1(4),
    Dk = DomainTuple1(5),
    twopi = 2.0*pi,
    invNg3 = 1.0/Ng:real**3;

  // Start by looping over dimensions
  var gridrms: real = 0.0;
  const Dreal = DomainTuple1(2);
  timeit.clear(); timeit.start();
  for idim in 1..Ndim {
    // Compute the displacements here
    forall (ik, ire, iim) in zip(Dk, Dre, Dim) {
      local {
        var kk = kFreq(ik, Ng);
        const ki = kk(idim)*twopi;
        AA[ire] = -ki * BB[iim];
        AA[iim] =  ki * BB[ire];
      }
    }
    /* Note that no additional normalization is required here */
    AA.fftReverse();
    AA.updateGhosts();

    gridrms += + reduce (AA[Dreal]**2);

    /* Now pull the appropriate density back onto the particles */
    interpolateFromGrid(PP, AA, PP.p(idim));

    // End loop over dimensions
  }
  timeit.stop();
  writef("Time to compute displacements: %r (s)\n", timeit.elapsed());

  writef("The RMS displacement on the grid (in cells) : %r \n", sqrt(gridrms*invNg3)*Ng);


  // Update positions and momenta
  timeit.clear(); timeit.start();
  const tfac = timeintegrand(log(ainit)):real(32); /* Use the global ainit */
  const fom = 1.0:real(32); /* Should be good enough at high redshift */
  var maxdisp, rmsdisp: real(32); // Maxdisp is absolute, so zero is a fine initial value
  // Loop over dimensions
  for idim in 1..Ndim {
    // Unfortunately, for loops don't support reductions...
    var rmsdisp1, maxdisp1 : real(32);
    forall (x1, p1) in zip(PP.r(idim), PP.p(idim)) with (+ reduce rmsdisp1, max reduce maxdisp1) {
      local {
      rmsdisp1 += p1**2;
      if (abs(p1) > maxdisp1) then maxdisp1=p1;
      x1 = periodic(x1+p1);
      p1 *= tfac*fom;
      }
    }
    // Unfortunately, for loops don't support reductions...
    rmsdisp += rmsdisp1;
    if (maxdisp1 > maxdisp) then maxdisp=maxdisp1;
  }
  timeit.stop();
  writef("Time to update particle positions: %r (s)\n", timeit.elapsed());

  writef("Maximum displacement %r grid cells\n", maxdisp*Ng);
  rmsdisp = sqrt(rmsdisp/Npart:real(32));
  writef("RMS displacement %r grid cells\n", rmsdisp*Ng);

}

proc streamParticles(logai: real, logaf: real) {
  const tfac = (symx(logaf) - symx(logai)):real(32);
  for param idim in 1..Ndim {
    [(x1, p1) in zip(PP.r(idim), PP.p(idim))] x1 = periodic(x1+p1*tfac);
  }
}

proc kickParticles(logai: real, logaf: real) {
  const tfac = (symp(logaf) - symp(logai)):real(32);
  for param idim in 1..Ndim {
    [(p1, f1) in zip(PP.p(idim), PP.f(idim))] p1 += f1*tfac;
  }
}

proc pmforce(aa: real) {
  var tt : Timer();
  tt.clear(); tt.start();

  // Compute some useful numbers
  const scale = 1.0/Npart;
  const Ea2 = omegam/(aa**3) + omegak/(aa**2) + omegax*exp(-3.0*(1+weff)*log(aa));
  const om = omegam/aa**3/Ea2;

  // Put the particles onto the grid
  AA = 0.0;
  CIC(PP, AA);
  writeln("PM Force Phase 1:", tt.elapsed());

  // Work out phi
  const Dre = DomainTuple1(3),
    Dim = DomainTuple1(4),
    Dk = DomainTuple1(5),
    twopi = 2.0*pi,
    twopi2 = twopi**2;

  AA.fftForward(transposeOpt=true);
  forall (ik, ire, iim) in zip(Dk, Dre, Dim){
    local { // For safety
      const ikt = (ik(2), ik(1), ik(3)); // Transpose 
      const kk = kFreq(ikt, Ng);
      var k2 = 0.0;
      for param idim in 1..Ndim do k2 += kk[idim]**2;
      k2 *= twopi2;
      if (k2 > 0) {
        const fac = -(1.0/k2)*1.5*om*scale;
        BB.localAccess[ire] = fac*AA.localAccess[ire];
        BB.localAccess[iim] = fac*AA.localAccess[iim];
      } else {
        BB.localAccess[ire] = 0.0;
        BB.localAccess[iim] = 0.0;
      }
    } // End of local
  }
  BB.fftReverse(transposeOpt=true);
  BB.updateGhosts();
  writeln("PM Force Phase 2:", tt.elapsed());

  // Now finite difference in each direction
  const Dreal = DomainTuple1(2);
  for idim in 1..Ndim {
    var tmp = (0,0,0);
    tmp(idim) = 1;
    const dir = tmp;
    forall idx in Dreal {
      local {
        // 4-pt difference scheme, do this by hand
        var plus1 = idx + dir;
        var plus2 = plus1 + dir;
        var minus1 = idx - dir;
        var minus2 = minus1 - dir;

        // Now handle periodic boundary conditions for dims 2 and 3
        if (idim != 1) {
          plus1(idim) = plus1(idim)%Ng;
          plus2(idim) = plus2(idim)%Ng;
          minus1(idim) = (minus1(idim)+Ng)%Ng;
          minus2(idim) = (minus2(idim)+Ng)%Ng;
        }

        var p1 = BB.localAccess[plus1],
          p2 = BB.localAccess[plus2],
          m1 = BB.localAccess[minus1],
          m2 = BB.localAccess[minus2];

        AA.localAccess[idx] = (-2.0/3.0)*(p1-m1)+(1.0/12.0)*(p2-m2);
        AA.localAccess[idx] *= Ng;
      }
    }
    AA.updateGhosts();

    // Pull the forces from the grid to the particles
    interpolateFromGrid(PP, AA, PP.f(idim));

    // End loop over dimensions
  }
  writeln("PM Force Phase 3:", tt.elapsed());

}

